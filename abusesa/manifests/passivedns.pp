# Install abusesa-passivedns.
#
class abusesa::passivedns(
  $datadir='/var/lib/passivedns',
) {

  if ! $abusesa_passivedns_package {
    if $::abusesa_passivedns_package_latest {
      $abusesa_passivedns_package = $::abusesa_passivedns_package_latest
    } else {
      fail('Must define $abusesa_passivedns_package or $abusesa_passivedns_package_latest')
    }
  }

  include user::system
  realize(User['passdns'], Group['passdns'])

  exec { 'usermod-abusesa-passivedns':
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => 'usermod -a -G passdns abusesa',
    unless  => 'id -n -G abusesa | grep \'\bpassdns\b\'',
    require => [
      User['abusesa'],
      Group['passdns'],
    ],
  }

  if $datadir != '/var/lib/passivedns' {
    file { '/var/lib/passivedns':
      ensure => link,
      target => $datadir,
    }
  }

  file { $datadir:
    ensure => directory,
    mode   => '2770',
    owner  => 'passdns',
    group  => 'passdns',
  }

  file { [
    '/var/lib/passivedns/log',
    '/var/lib/passivedns/storage',
  ]:
    ensure => directory,
    mode   => '2770',
    owner  => 'passdns',
    group  => 'passdns',
  }

  file { '/var/lib/passivedns/.profile':
    ensure  => present,
    mode    => '0600',
    owner   => 'passdns',
    group   => 'passdns',
    content => "umask 007\n",
  }

  file { '/usr/local/src/abusesa-passivedns.tar.gz':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => $::operatingsystem ? {
      'openbsd' => 'wheel',
      default   => 'root',
    },
    source => "puppet:///files/packages/${abusesa_passivedns_package}",
  }
  util::extract::tar { '/usr/local/src/abusesa-passivedns':
    ensure  => latest,
    strip   => '1',
    source  => '/usr/local/src/abusesa-passivedns.tar.gz',
    require => File['/usr/local/src/abusesa-passivedns.tar.gz'],
    before  => Python::Setup::Install['/usr/local/src/abusesa-passivedns'],
  }

  if $::operatingsystem in ['CentOS','RedHat'] and versioncmp($::operatingsystemrelease, '6') < 0 {
    include python::python26
    python::setup::install { '/usr/local/src/abusesa-passivedns':
      python  => 'python2.6',
      require => Package['python26'],
    }
  } else {
    python::setup::install { '/usr/local/src/abusesa-passivedns': }
  }

}
