# Configure AbuseSA services.
#
class abusesa::services(
  $allow_dn=undef,
  $services=[],
  $socketdir='/var/lib/abuserv/run',
) {

  include user::system
  realize(User['abuserv'], Group['abuserv'])

  exec { 'usermod-abusesa-abuserv':
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => 'usermod -a -G abuserv abusesa',
    unless  => 'id -n -G abusesa | grep \'\babuserv\b\'',
    require => [
      User['abusesa'],
      Group['abuserv'],
    ],
  }

  include nginx

  exec { 'usermod-nginx-abuserv':
    path    => '/bin:/usr/bin:/sbin:/usr/sbin',
    command => "usermod -a -G abuserv ${nginx::user}",
    unless  => "id -n -G ${nginx::user} | grep '\\babuserv\\b'",
    notify  => Service['nginx'],
    require => [
      Package['nginx'],
      Group['abuserv'],
    ],
  }

  file { [
    '/var/lib/abuserv',
    '/var/lib/abuserv/run',
  ]:
    ensure => directory,
    mode   => '2770',
    owner  => 'abuserv',
    group  => 'abuserv',
  }

  file { '/var/lib/abuserv/.profile':
    ensure  => present,
    mode    => '0600',
    owner   => 'abuserv',
    group   => 'abuserv',
    content => "umask 007\n",
  }

  nginx::config { 'abusesa.conf':
    content => template('abusesa/services/nginx.conf.erb'),
  }

}
