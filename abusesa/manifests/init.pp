# Install AbuseSA.
#
# === Parameters
#
# $datadir:
#   AbuseSA home directory. Defaults to /var/lib/abusesa.
#
# $botnets:
#   Array of botnet paths to start at boot.
#
# $botuser:
#   User to run bots as. Defaults to abusesa.
#
class abusesa(
  $datadir='/var/lib/abusesa',
  $botnets=undef,
  $botuser='abusesa',
) {

  if ! $abusesa_package {
    if $::abusesa_package_latest {
      $abusesa_package = $::abusesa_package_latest
    } else {
      fail('Must define $abusesa_package or $abusesa_package_latest')
    }
  }

  include user::system
  realize(User['abusesa'], Group['abusesa'])

  if $datadir != '/var/lib/abusesa' {
    file { '/var/lib/abusesa':
      ensure => link,
      target => $datadir,
    }
  }

  file { $datadir:
    ensure => directory,
    mode   => '2770',
    owner  => 'abusesa',
    group  => 'abusesa',
  }

  file { '/var/lib/abusesa/.profile':
    ensure  => present,
    replace => false,
    mode    => '0600',
    owner   => 'abusesa',
    group   => 'abusesa',
    content => "umask 007\n",
  }

  file { '/usr/local/src/abusesa.tar.gz':
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => $::operatingsystem ? {
      'openbsd' => 'wheel',
      default   => 'root',
    },
    source => "puppet:///files/packages/${abusesa_package}",
  }
  util::extract::tar { '/usr/local/src/abusesa':
    ensure  => latest,
    strip   => '1',
    source  => '/usr/local/src/abusesa.tar.gz',
    require => File['/usr/local/src/abusesa.tar.gz'],
    before  => Python::Setup::Install['/usr/local/src/abusesa'],
}

  if $::operatingsystem in ['CentOS','RedHat'] and versioncmp($::operatingsystemrelease, '6') < 0 {
    include python::python26
    python::setup::install { '/usr/local/src/abusesa':
      python  => 'python2.6',
      require => Package['python26'],
    }
  } else {
    python::setup::install { '/usr/local/src/abusesa': }
  }

  if $botnets {
    class { 'abusehelper::init':
      botnets => $botnets,
      botuser => $botuser,
    }
  }

}


# Create AbuseSA htdocs root.
#
define abusesa::configwebhost() {

  file { "/srv/www/https/${name}/abusesa":
    ensure => directory,
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }
  file { "/srv/www/https/${name}/abusesa/index.html":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    content => '',
  }

}
