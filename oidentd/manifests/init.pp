# Install oidentd service
#
# === Parameters:
#
#     $config:
#         Source to oidentd.conf. Default is to install empty config.
#
class oidentd($config=undef) {

    package { "oidentd":
        ensure => installed,
    }

    file { "/etc/oidentd.conf":
        ensure  => present,
        source  => $config,
        content => $config ? {
            undef   => "",
            default => undef,
        },
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["oidentd"],
        notify  => Service["oidentd"],
    }

    if $::operatingsystem == "OpenBSD" {
        file { "/etc/rc.d/oidentd":
            ensure => present,
            mode   => "0555",
            owner  => "root",
            group  => "bin",
            source => "puppet:///modules/oidentd/oidentd.rc",
            before => Service["oidentd"],
        }
    }

    service { "oidentd":
        ensure => running,
        enable => true,
        start  => $::operatingsystem ? {
            "openbsd" => "/usr/local/sbin/oidentd -e -u _identd -g _identd",
            default   => undef,
        },
    }

}
