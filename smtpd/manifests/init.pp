# Configure smtpd.
#
# === Parameters
#
# $maildomain:
#   Domain to masquerade as.
#
# $mailserver:
#   Server to relay mail via.
#
# $listen:
#   Listen on external interfaces. Defaults to false.
#
# $gecos:
#   Boolean for whether to enable gecos aliases.
#   Defaults to true.
#
# $maildir:
#   Directory in user home for INBOX. Defaults to "Mail".
#
# $config:
#   Path to custom configuration file.
#
# $custom:
#   Array of custom accept/reject rules.
#
# $domains:
#   Array of primary domains to accept mail for.
#
# $virtuals:
#   Array of virtual domains to accept mail for.
#
# $ssl_key:
#   Source path of private key.
#
# $ssl_cert:
#   Source path of certificate.
#
class smtpd(
  $maildomain=undef,
  $mailserver=undef,
  $listen=false,
  $gecos=true,
  $maildir='Mail',
  $config=undef,
  $custom=undef,
  $domains=undef,
  $virtuals=undef,
  $ssl_key="${::puppet_ssldir}/private_keys/${::homename}.pem",
  $ssl_cert="${::puppet_ssldir}/certs/${::homename}.pem"
) {

  if $listen == true and $::operatingsystem != 'OpenBSD' {
    fail('listen only supported on OpenBSD')
  }

  case $::operatingsystem {
    'centos','redhat': {
      if versioncmp($::operatingsystemrelease, '6') < 0 {
        fail("smtpd requires atleast ${::operatingsystem} 6")
      }

      $package = 'opensmtpd'
      $service = 'opensmtpd'
      $confdir = '/etc/opensmtpd'
      $aliases = '/etc/aliases'
      $mda = undef

      package { $package:
        ensure => installed,
        before => File["${confdir}/smtpd.conf"],
      }

      exec { '/usr/sbin/alternatives --set mta /usr/sbin/sendmail.opensmtpd':
        refreshonly => true,
        subscribe   => Package[$package],
        before      => Service[$service],
      }

      service { [ 'postfix', 'sendmail' ]:
        ensure => stopped,
        enable => false,
        before => Service[$service],
      }
    }
    'ubuntu': {
      if versioncmp($::operatingsystemrelease, '14.04') < 0 {
        fail("smtpd requires atleast ${::operatingsystem} 14.04")
      }

      $package = 'opensmtpd'
      $service = 'opensmtpd'
      $confdir = '/etc'
      $aliases = '/etc/aliases'
      $mda = undef

      package { $package:
        ensure => installed,
        before => File["${confdir}/smtpd.conf"],
      }
    }
    'openbsd': {
      $package = undef
      $service = 'smtpd'
      $confdir = '/etc/mail'
      $aliases = '/etc/mail/aliases'
      $mda = '/usr/local/bin/procmail -Y -t -f %{sender}'

      file { '/etc/mailer.conf':
        ensure => present,
        mode   => '0644',
        owner  => 'root',
        group  => 'wheel',
        source => 'puppet:///modules/smtpd/mailer.conf',
        before => Service[$service],
      }

      service { 'sendmail':
        ensure => stopped,
        enable => false,
        before => Service[$service],
      }
    }
    default: {
      fail("smtpd not supported on ${::operatingsystem}")
    }
  }

  if $mailserver {
    $mailrelay = "smtp+tls://${mailserver}"
  } else {
    $mailrelay = undef
  }

  include ssl

  if $config {
    $content = undef
  } else {
    $content = $listen ? {
      true    => template('smtpd/server.conf.erb'),
      default => template('smtpd/client.conf.erb'),
    }
  }

  file { "${confdir}/smtpd.conf":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => $::operatingsystem ? {
      'openbsd' => 'wheel',
      default   => 'root',
    },
    source  => $config,
    content => $content,
    notify  => Service[$service],
  }

  service { $service:
    ensure => running,
    enable => true,
    start  => $::operatingsystem ? {
      'openbsd' => '/usr/sbin/smtpd',
      default   => undef,
    },
  }

  if $listen == true or $config {
    file { "${ssl::private}/smtpd.key":
      ensure => present,
      mode   => '0600',
      owner  => 'root',
      group  => 'wheel',
      source => $ssl_key,
      notify => Service['smtpd'],
    }
    file { "${ssl::certs}/smtpd.crt":
      ensure => present,
      mode   => '0644',
      owner  => 'root',
      group  => 'wheel',
      source => $ssl_cert,
      notify => Service['smtpd'],
    }
  }

  if $listen == true {
    include procmail

    procmail::rc { '00-default.rc':
      content => "MAILDIR=\$HOME/${maildir}\nDEFAULT=\$MAILDIR/INBOX\n",
    }

    file { [ "/root/${maildir}", "/etc/skel/${maildir}" ]:
      ensure => directory,
      mode   => '0700',
      owner  => 'root',
      group  => 'wheel',
      before => Service['smtpd'],
    }

    if $gecos == true {
      file { '/usr/local/sbin/generate-smtpd-gecos.sh':
        ensure => present,
        mode   => '0700',
        owner  => 'root',
        group  => 'wheel',
        source => 'puppet:///modules/smtpd/generate-smtpd-gecos.sh',
      }
      exec { '/usr/local/sbin/generate-smtpd-gecos.sh':
        unless  => '/bin/test /etc/mail/gecos -nt /etc/passwd',
        require => File['/usr/local/sbin/generate-smtpd-gecos.sh'],
        notify  => Exec['makemap aliases'],
      }
    }

    file { '/etc/mail/aliases':
      ensure => present,
      mode   => '0644',
      owner  => 'root',
      group  => 'wheel',
      source => [
        "puppet:///files/mail/aliases.${::homename}",
        'puppet:///files/mail/aliases',
      ],
    }
    exec { 'makemap aliases':
      command     => $gecos ? {
        false => 'makemap aliases',
        true  => 'cat aliases gecos > aliases.gecos && makemap -o aliases.db aliases.gecos',
      },
      refreshonly => true,
      cwd         => '/etc/mail',
      path        => '/bin:/usr/bin:/sbin:/usr/sbin',
      subscribe   => File['/etc/mail/aliases'],
      before      => Service['smtpd'],
    }

    file { '/etc/mail/clients':
      ensure => present,
      mode   => '0644',
      owner  => 'root',
      group  => 'wheel',
      source => [
        "puppet:///files/mail/clients.${::homename}",
        'puppet:///files/mail/clients',
        'puppet:///modules/smtpd/empty',
      ],
    }
    exec { 'makemap -t set clients':
      refreshonly => true,
      cwd         => '/etc/mail',
      path        => '/bin:/usr/bin:/sbin:/usr/sbin',
      subscribe   => File['/etc/mail/clients'],
      before      => Service['smtpd'],
    }

    if $domains {
      smtpd::aliases { $domains:
        gecos     => $gecos,
        subscribe => $gecos ? {
          false => undef,
          true  => Exec['/usr/local/sbin/generate-smtpd-gecos.sh'],
        },
      }
    }

    if $virtuals {
      smtpd::virtual { $virtuals: }
    }
  }

}


# Install alias mapping for domain.
#
define smtpd::aliases($gecos) {

  file { "/etc/mail/aliases.${name}":
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'wheel',
    source => [
      "puppet:///files/mail/aliases.${name}",
      "puppet:///files/mail/aliases.${::homename}",
      'puppet:///files/mail/aliases',
    ],
  }
  exec { "makemap aliases.${name}":
    command     => $gecos ? {
      false => "makemap aliases.${name}",
      true  => "cat aliases.${name} gecos > aliases.${name}.gecos && makemap -o aliases.${name}.db aliases.${name}.gecos",
    },
    refreshonly => true,
    cwd         => '/etc/mail',
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    subscribe   => File["/etc/mail/aliases.${name}"],
    before      => Service['smtpd'],
  }

}


# Install virtual user mapping for domain.
#
define smtpd::virtual() {

  file { "/etc/mail/virtual.${name}":
    ensure => present,
    mode   => '0644',
    owner  => 'root',
    group  => 'wheel',
    source => [
      "puppet:///files/mail/virtual.${name}",
      "puppet:///files/mail/virtual.${::homename}",
      'puppet:///files/mail/virtual',
    ],
  }
  exec { "makemap virtual.${name}":
    refreshonly => true,
    cwd         => '/etc/mail',
    path        => '/bin:/usr/bin:/sbin:/usr/sbin',
    subscribe   => File["/etc/mail/virtual.${name}"],
    before      => Service['smtpd'],
  }

}
