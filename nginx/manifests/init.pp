# Install and configure nginx.
#
class nginx(
  $workers=$::processorcount,
) {

  case $::operatingsystem {
    'openbsd': {
      if $::kernelversion == '5.6' {
        $user = 'www'
        $group = 'www'
      } else {
        $user = '_nginx'
        $group = '_nginx'
      }
      $logdir = '/var/www/logs'
    }
    'ubuntu': {
      $user = 'www-data'
      $group = 'www-data'
      $logdir = '/var/log/nginx'
    }
    default: {
      $user = 'nginx'
      $group = 'nginx'
      $logdir = '/var/log/nginx'
    }
  }

  if ! ($::operatingsystem == 'OpenBSD' and $::kernelversion == '5.6') {
    package { 'nginx':
      ensure => installed,
      before => File['/etc/nginx/nginx.conf', '/etc/nginx/conf.d'],
    }
  }

  file { '/etc/nginx/nginx.conf':
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => $::operatingsystem ? {
      'openbsd' => 'wheel',
      default   => 'root',
    },
    content => template('nginx/nginx.conf.erb'),
    notify  => Service['nginx'],
  }

  file { '/etc/nginx/conf.d':
    ensure  => directory,
    mode    => '0644',
    owner   => 'root',
    group   => $::operatingsystem ? {
      'openbsd' => 'wheel',
      default   => 'root',
    },
    purge   => true,
    force   => true,
    recurse => true,
    source  => 'puppet:///modules/custom/empty',
    notify  => Service['nginx'],
  }

  service { 'nginx':
    ensure => running,
    enable => true,
    start  => $::operatingsystem ? {
      'openbsd' => '/usr/sbin/nginx -u',
      default   => undef,
    },
  }

}


# Add nginx configuration file.
#
define nginx::config(
  $source=undef,
  $content=undef,
) {

  file { "/etc/nginx/conf.d/${name}":
    ensure  => present,
    mode    => '0644',
    owner   => 'root',
    group   => $::operatingsystem ? {
      'openbsd' => 'wheel',
      default   => 'root',
    },
    content => $content,
    source  => $source,
    notify  => Service['nginx'],
  }

}
