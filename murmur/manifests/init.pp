# Install murmur (Mumble server).
#
# === Parameters
#
#   $ssl_key:
#       Path to SSL private key. Defaults to puppet node key.
#
#   $ssl_cert:
#       Path to SSL certificate. Defaults to puppet node certificate.
#
#   $password:
#       Server passwords. Defaults to none.
#
#   $welcome:
#       Server welcome message.
#
class murmur(
    $package=$::murmur_package_latest,
    $ssl_key="${::puppet_ssldir}/private_keys/${::homename}.pem",
    $ssl_cert="${::puppet_ssldir}/certs/${::homename}.pem",
    $password="",
    $welcome="<br />Welcome to this server running Murmur.<br />",
) {

    if ! $package {
        fail("Must define parameter package or \$murmur_package_latest")
    }

    file { "/usr/local/src/murmur-static_x86.tar.bz2":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => "puppet:///files/packages/${package}",
    }
    util::extract::tar { "/usr/local/murmur":
        ensure  => latest,
        strip   => 1,
        source  => "/usr/local/src/murmur-static_x86.tar.bz2",
        require => File["/usr/local/src/murmur-static_x86.tar.bz2"],
        notify  => Service["murmur"],
    }

    include user::system
    realize(User["murmur"], Group["murmur"])

    include ssl
    file { "${ssl::certs}/murmur.crt":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
        source => $ssl_cert,
        notify => Service["murmur"],
    }
    file { "${ssl::private}/murmur.key":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "murmur",
        source  => $ssl_key,
        require => Group["murmur"],
        notify  => Service["murmur"],
    }

    file { "/srv/murmur":
        ensure  => directory,
        mode    => "0700",
        owner   => "murmur",
        group   => "murmur",
        require => User["murmur"],
    }
    file { "/srv/murmur/murmur.ini":
        ensure  => present,
        mode    => "0600",
        owner   => "murmur",
        group   => "murmur",
        content => template("murmur/murmur.ini.erb"),
        require => File["/srv/murmur"],
        notify  => Service["murmur"],
    }

    file { "/etc/init.d/murmur":
        ensure => present,
        mode   => "0755",
        owner  => "root",
        group  => "root",
        source => "puppet:///modules/murmur/murmur.init",
        notify => Exec["add-service-murmur"],
    }
    exec { "add-service-murmur":
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        command     => "chkconfig --add murmur",
        refreshonly => true,
        before      => Service["murmur"],
    }
    service { "murmur":
        ensure => running,
        enable => true,
    }

}
