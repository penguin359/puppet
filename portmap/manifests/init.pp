# Install RPC portmapper service.
#
class portmap::server {

    case $::operatingsystem {
        "openbsd": {
            $package = ""
            $service = "portmap"
        }
        "centos","redhat": {
            case $::operatingsystemrelease {
                /^[1-5]\./: {
                    $package = "portmap"
                    $service = "portmap"
                }
                default: {
                    $package = "rpcbind"
                    $service = "rpcbind"
                }
            }
        }
        "fedora": {
            $package = "rpcbind"
            $service = "rpcbind"
        }
        "ubuntu": {
            $package = "rpcbind"
            if (versioncmp($::operatingsystemrelease, "13.10") >= 0) {
                $service = "rpcbind"
            } else {
                $service = "portmap"
            }
        }
        default: {
            fail("portmap::server not supported on ${::operatingsystem}")
        }
    }

    if $package {
        package { "portmap":
            ensure => installed,
            name   => $package,
            before => Service["portmap"],
        }
    }

    service { "portmap":
        ensure => running,
        name   => $service,
        start  => $::operatingsystem ? {
            "openbsd" => "/usr/sbin/portmap",
            default   => undef,
        },
        enable => true,
    }

}
