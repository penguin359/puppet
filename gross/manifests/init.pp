
# Install and configure Grossd.
#
#   $protocol:
#     Activates the server protocols grossd will support. Valid
#     protocols are 'sjsms', 'postfix' and 'milter'.
#
#   $number_buffers:
#     The number of filters used in the ring queue raising this value
#     will cause an entry to stay in the servers' memory longer.
#
#   $rotate_interval:
#     The number of seconds between filter rotation. Let
#     N := 'number_buffers' and I := 'rotate_interval'. An entry will stay
#     in the servers' memory for (N - 0.5) * I seconds in average.
#
#   $grey_delay:
#     The time in seconds new triplets are kept on the greylist.
#
#   $block_treshold:
#     The threshold after which grossd sends a permanent error to the
#     client. Every check that considers client_ip as suspicious
#     returns a value (check weight). When sum of these values gets
#     equivalent or greater than 'block_threshold', grossd sends a
#     STATUS_BLOCK response.
#
#   $dnsbl:
#     Dns domain name of the dnsbl that 'dnsbl' check will
#     query. There are no defaults, but below is a list of dnsbls you
#     could be using you may assign different weights for the dnsbl's,
#     default weight is 1.
#     Syntax: dnsbl = [ 'bl.spamcop.net;2', 'dnsbl.sorbs.net' ]
#
#   $dnswl:
#    Analogous to 'dnsbl'. Remember that dnswl is a *definitive*
#    check, that is grossd waits for the check to complete before
#    deciding how to respond. This may cause unwanted latency. Highly
#    recommended if you use grossd as a traditional greylister.
#
#  $rpm:
#    Location of RPM package.
#
class gross(
  $protocol        = 'milter',
  $number_buffers  = '8',
  $rotate_interval = '3600',
  $grey_delay      = '10',
  $block_treshold  = '0',
  $dnsbl           = [],
  $dnswl           = [],
  $rpm             =
  'https://bitbucket.org/oherrala/gross/downloads/gross-1.0.2-1.el6.x86_64.rpm'
){

  package { 'gross':
    ensure   => 'installed',
    provider => 'rpm',
    source   => $rpm,
  }

  service { 'gross':
    ensure  => 'running',
    enable  => true,
    require => Package['gross'],
  }

  file { '/var/db/grossd/':
    ensure => 'directory',
    owner  => 'nobody',
    group  => 'root',
    mode   => '0750',
    before => Service['gross'],
  }

  file { '/etc/grossd.conf':
    ensure  => 'present',
    owner   => 'root',
    group   => 'root',
    mode    => '0444',
    content => template('gross/grossd.conf.erb'),
    before  => Service['gross'],
    notify  => Service['gross'],
  }
}
