# Install Lynx WWW browser
#
# === Global variables:
#
#   $www_default_url:
#       Default starting URL.
#
class lynx {

    if ! $::www_default_url {
        $www_default_url = "http://www.${::domain}"
    }

    if $::operatingsystem != "OpenBSD" {
        package { "lynx":
            ensure => installed,
            name   => $::operatingsystem ? {
                "ubuntu"  => "lynx-cur",
                default   => "lynx",
            },
            before => File["/usr/local/bin/html2text"],
        }
    }

    if $::operatingsystem == "OpenBSD" {
        exec { "add-local-lynx-config":
             command => "echo 'INCLUDE:/etc/lynx-site.cfg' >> /etc/lynx.cfg",
             path    => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
             user    => root,
             unless  => "fgrep 'INCLUDE:/etc/lynx-site.cfg' /etc/lynx.cfg",
        }
    }

    file { "lynx-site.cfg":
        ensure  => present,
        name    => $::operatingsystem ? {
            "ubuntu" => "/etc/lynx-cur/local.cfg",
            default  => "/etc/lynx-site.cfg",
        },
        content => template("lynx/lynx-site.cfg.erb"),
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => $::operatingsystem ? {
            "openbsd" => undef,
            default   => Package["lynx"],
        },
    }

    file { "/usr/local/bin/html2text":
        ensure => present,
        source => "puppet:///modules/lynx/html2text",
        mode   => "0755",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

}
