
# Install spamassassin common components
#
class spamassassin::common {

    package { "spamassassin":
        ensure => present,
        name   => $::operatingsystem ? {
            "openbsd" => "p5-Mail-SpamAssassin",
            default   => "spamassassin",
        },
    }

    file { "/etc/mail/spamassassin/local.cf":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        source  => [
            "puppet:///files/mail/spamassassin.cf.${::homename}",
            "puppet:///files/mail/spamassassin.cf",
            "puppet:///modules/spamassassin/local.cf",
        ],
        notify  => Service["spamassassin"],
        require => Package["spamassassin"],
    }

    service { "spamassassin":
        ensure    => running,
        enable    => true,
        binary    => $::operatingsystem ? {
            "openbsd" => "/usr/local/bin/spamd -d",
            default   => undef,
        },
        start     => $::operatingsystem ? {
            "openbsd" => "/usr/local/bin/spamd -d -c -H -m 5",
            default   => undef,
        },
        hasstatus => true,
        require   => Package["spamassassin"],
    }

}


# Install spamassassin using procmail
#
class spamassassin::procmail {

    include spamassassin::common
    include procmail

    $spamc = $::operatingsystem ? {
        "openbsd" => "/usr/local/bin/spamc",
        default   => "/usr/bin/spamc",
    }

    procmail::rc { "10-spamassassin.rc":
        content => ":0fw\n| ${spamc}\n",
    }

}


# Install spamassassin milter
#
# === Parameters
#
#   $options:
#       Extra options for spamass-milter.
#
class spamassassin::milter($options="") {

    include spamassassin::common

    $sendmail_milter = [
        # filter name
        'spamass',
        # filter options
        'S=unix:/var/run/spamass-milter/spamass-milter.sock, F=, T=C:15m;S:4m;R:4m;E:10m',
    ]

    package { "spamass-milter":
        ensure => installed,
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            augeas { "set-spamass-milter-sysconfig":
                changes => "set EXTRA_FLAGS '\"${options}\"'",
                incl    => "/etc/sysconfig/spamass-milter",
                lens    => "Shellvars.lns",
                require => Package["spamass-milter"],
                notify  => Service["spamass-milter"],
            }
        }
        default: {
            fail("spamassassin::milter not supported on ${::operatingsystem}")
        }
    }

    service { "spamass-milter":
        ensure => running,
        enable => true,
    }

}


# Install ClamAV plugin for spamassassin
#
class spamassassin::plugin::clamav {

    require clamav::daemon
    include spamassassin::common

    file { "/etc/mail/spamassassin/ClamAV.pm":
        ensure => present,
        source => "puppet:///modules/spamassassin/ClamAV.pm",
        mode   => "0644",
        owner  => "root",
        group  => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
    }

    file { "/etc/mail/spamassassin/clamav.cf":
        ensure  => present,
        source  => "puppet:///modules/spamassassin/clamav.cf",
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => [ File["/etc/mail/spamassassin/ClamAV.pm"],
                     Package["spamassassin"], ],
        notify  => Service["spamassassin"],
    }

}


# Install iXhash plugin for spamassassin
#
class spamassassin::plugin::ixhash {

    include spamassassin::common

    package { "spamassassin-iXhash2":
        ensure => installed,
        notify => Service["spamassassin"],
    }

}


# Install pyzor plugin for spamassassin
#
class spamassassin::plugin::pyzor {

    include spamassassin::common

    package { "pyzor":
        ensure => installed,
    }

    exec { "pyzor --homedir /etc/mail/pyzor discover":
        user    => "root",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        creates => "/etc/mail/pyzor/servers",
        require => Package["pyzor"],
        before  => [ File["/etc/mail/pyzor"],
                     File["/etc/mail/pyzor/servers"], ],
    }
    file { "/etc/mail/pyzor":
        ensure => directory,
        mode   => "0755",
        owner  => "root",
        group  => "root",
    }
    file { "/etc/mail/pyzor/servers":
        ensure => present,
        mode   => "0644",
        owner  => "root",
        group  => "root",
    }

    file { "/etc/mail/spamassassin/pyzor.cf":
        content => "pyzor_options --homedir /etc/mail/pyzor\n",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => File["/etc/mail/pyzor/servers"],
        notify  => Service["spamassassin"],
    }

}
