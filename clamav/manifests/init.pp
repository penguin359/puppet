
# Install Clam AntiVirus
#
# === Parameters
#
#     $update:
#         Set to false to disable automatic virus database updates.
#
class clamav($update=true) {

    package { "clamav":
        ensure => installed,
    }

    case $::operatingsystem {
        "centos","redhat": {
            if (versioncmp($::operatingsystemrelease, 7) < 0) {
                if (!$update) {
                    file { "/etc/cron.daily/freshclam":
                        ensure => absent,
                    }
                }
            } else {
                package { "clamav-update":
                    ensure => $update ? {
                        true  => "present",
                        false => "absent",
                    }
                }
                if $update {
                    augeas { "enable-freshclam":
                        context => "/files/etc/sysconfig/freshclam",
                        changes => "set FRESHCLAM_DELAY ''",
                        require => Package["clamav-update"],
                    }
                    exec { "sed -i 's/^Example$/#Example/' /etc/freshclam.conf":
                        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                        user    => "root",
                        onlyif  => "egrep '^Example$' /etc/freshclam.conf",
                        require => Package["clamav-update"],
                    }
                }
            }
        }
        default: {
            fail("clamav module not supported on ${::operatingsystem}")
        }
    }

}


# Install Clam Antivirus Daemon
#
class clamav::daemon {

    require clamav

    case $::operatingsystem {
        "centos","redhat": {
            if (versioncmp($::operatingsystemrelease, 7) < 0) {
                $package = "clamd"
                $service = "clamd"
            } else {
                $package = "clamav-scanner-systemd"
                $service = "clamd@scan"
                exec { "sed -i 's/^Example$/#Example/' /etc/clamd.d/scan.conf":
                    path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                    user    => "root",
                    onlyif  => "egrep '^Example$' /etc/clamd.d/scan.conf",
                    require => Package["clamd"],
                    notify  => Service["clamd"],
                }
                exec { "sed -i 's/^#LocalSocket /LocalSocket /' /etc/clamd.d/scan.conf":
                    path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                    user    => "root",
                    onlyif  => "egrep '#LocalSocket ' /etc/clamd.d/scan.conf",
                    require => Package["clamd"],
                    notify  => Service["clamd"],
                }
                file { "/etc/clamd.conf":
                    ensure  => link,
                    target  => "/etc/clamd.d/scan.conf",
                    owner   => "root",
                    group   => "root",
                    require => Package["clamd"],
                }
            }
        }
    }

    package { "clamd":
        ensure => installed,
        name   => $package,
    }

    service { "clamd":
        ensure  => running,
        name    => $service,
        enable  => true,
        require => Package["clamd"],
    }

}


# Scan directories periodically
#
# === Parameters:
#
#     $name:
#         Directory path to scan.
#     $hour:
#         At what hour should scanning occur. Defaults to 4am.
#     $weekday:
#         At what day should sacnning occur. For daily scanning you
#         can use "*". Defaults to Sunday.
#     $exclude:
#         Directories matching to this regex will be excluded from
#         scanning. Default is to scan everything.
#
# === Sample usage:
#
# clamav::scan { "/export/roles":
#     exclude => "/export/roles/[a-z]*/library/archive",
# }
#
define clamav::scan($hour="04", $weekday="Sunday", $exclude=undef) {

    require clamav

    if $exclude {
        $exclude_opts = "--exclude-dir='${exclude}'"
    }

    cron { "virusscan-${name}":
        command => "clamscan -r --infected --no-summary ${name} ${exclude_opts}",
        user    => "root",
        hour    => $hour,
        minute  => "00",
        weekday => $weekday,
    }

}
