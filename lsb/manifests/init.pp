# Add LSB Core system components
#
class lsb::core {

    if $::kernel != "Linux" {
        fail("LSB module is supported only in Linux systems")
    }

    case $::operatingsystem {
        "centos","fedora","redhat": {
            $pkgname = "redhat-lsb-core"
        }
        "ubuntu": {
            $pkgname = "lsb-core"
        }
        default: {
            fail("lsb::core not supported on ${::operatingsystem}"}
        }
    }

    package { "lsb-core":
        ensure => installed,
        name   => $pkgname,
    }

}


# Add LSB Desktop system components
#
class lsb::desktop {

    if $::kernel != "Linux" {
        fail("LSB module is supported only in Linux systems")
    }

    case $::operatingsystem {
        "centos","redhat": {
            $pkgname = "redhat-lsb-graphics"
        }
        "fedora": {
            $pkgname = "redhat-lsb-desktop"
        }
        "ubuntu": {
            $pkgname = "lsb-desktop"
        }
        default: {
            fail("lsb::desktop not supported on ${::operatingsystem}"}
        }
    }

    package { "lsb-desktop":
        ensure => installed,
        name   => $pkgname,
    }

}
