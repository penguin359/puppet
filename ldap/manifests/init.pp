
# Install and configure ldap authentication
#
# === Global variables
#
#   $ldap_server:
#       Array containing LDAP server URI's.
#
#   $ldap_basedn:
#       LDAP base DN.
#
#   $ldap_login_umask:
#       Default umask for LDAP users in OpenBSD, defaults to 077.
#
# === Parameters
#
#   $auth:
#       Authentication method to use for LDAP queries. Valid values
#       are anonymous, bind and gssapi. Default is anonymous.
#
#   $credentials:
#       Credentials for authentication. For simple bind use array
#       containig user dn and password. For gssapi use array containing
#       principal name.
#
#   $mapping:
#       Attribute mapping to use. Valid values are ad and rfc2307.
#       Default is rfc2307.
#
# === Sample usage
#
# class { "ldap::auth":
#     auth        => "bind",
#     credentials => [ "uid=user,dc=example,dc=com", "secret", ],
# }
#
# class { "ldap::auth":
#     auth        => "gssapi",
#     credentials => [ "MYHOST\$@EXAMPLE.COM" ],
#     mapping     => "ad",
# }
#
class ldap::auth(
    $auth="anonymous",
    $credentials=[],
    $mapping="rfc2307"
) inherits ldap::client {

    include pam::common

    tag("bootstrap")

    $ldap_uri = inline_template('<%= @ldap_server.join(" ") -%>')
    if regsubst($ldap_uri, '^(ldaps)://.*', '\1') == "ldaps"{
        $ssl = "on"
    } else {
        $ssl = "off"
    }

    case $auth {
        "anonymous": {}
        "bind": {
            if !$credentials[0] and !$credentials[1] {
                fail("no \$credentials argument set")
            }
        }
        "gssapi": {
            require kerberos::kstart
            require sasl::client
            if $credentials[0] {
                $principal = $credentials[0]
            } else {
                $principal = "host/${::homename}"
            }
        }
        default: {
            fail("unsupported auth value \"${auth}\"")
        }
    }

    case $::operatingsystem {
        "centos","redhat": {
            if versioncmp($::operatingsystemrelease, "7") < 0 {
                $member_attr = "uniqueMember"
            } else {
                $member_attr = "member"
            }
        }
        "fedora": {
            if versioncmp($::operatingsystemrelease, "18") < 0 {
                $member_attr = "uniqueMember"
            } else {
                $member_attr = "member"
            }
        }
        "ubuntu": {
            if versioncmp($::operatingsystemrelease, "12.04") < 0 {
                $member_attr = "uniqueMember"
            } else {
                $member_attr = "member"
            }
        }
    }

    if $::kernel == "Linux" {
        include nscd
    }

    case $::operatingsystem {
        "centos","fedora","redhat": {
            case $::operatingsystemrelease {
                default: {
                    package { "nss-pam-ldapd":
                        ensure => installed,
                    }
                    exec { "authconfig --enableldap --enableldapauth --ldapserver='${ldap_uri}' --ldapbasedn='${ldap_basedn}' --enableforcelegacy --update":
                        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                        unless  => 'cat /etc/sysconfig/authconfig | egrep "^USELDAPAUTH=yes$|^USELDAP=yes$" | wc -l | egrep "^2$"',
                        before  => [ File["/etc/nslcd.conf"],
                                     Augeas["pam-ldap-conf"],
                                     File["/etc/openldap/ldap.conf"], ],
                        require => Package["authconfig", "nss-pam-ldapd"],
                    }
                    file { "/etc/nslcd.conf":
                        ensure  => present,
                        content => template("ldap/nslcd.conf.erb"),
                        mode    => "0600",
                        owner   => "root",
                        group   => "root",
                        notify  => Service["nslcd"],
                    }
                    augeas { "pam-ldap-conf":
                        changes => [ "set ssl ${ssl}",
                                     "set pam_password exop",
                                     "rm tls_cacertdir", ],
                        incl    => "/etc/pam_ldap.conf",
                        lens    => "Spacevars.simple_lns",
                    }
                    if $auth == "gssapi" and $::operatingsystem != "Fedora" {
                        file { "/etc/init.d/nslcd":
                            ensure  => present,
                            source  => "puppet:///modules/ldap/nslcd.init.gssapi",
                            mode    => "0755",
                            owner   => "root",
                            group   => "root",
                            require => Package["nss-pam-ldapd"],
                            notify  => Service["nslcd"],
                        }
                        file { "/etc/sysconfig/nslcd":
                            ensure  => present,
                            content => "K5START_PRINCIPAL='${principal}'\n",
                            mode    => "0755",
                            owner   => "root",
                            group   => "root",
                            require => Package["nss-pam-ldapd"],
                            notify  => Service["nslcd"],
                        }
                    }
                    service { "nslcd":
                         ensure => running,
                         enable => true,
                         notify => Service["nscd"],
                    }
                }
                /^[1-5]\./: {
                    package { "nss_ldap":
                        ensure => installed,
                    }
                    exec { "authconfig --enableldap --enableldapauth --enableldapssl --ldapserver='${ldap_uri}' --ldapbasedn='${ldap_basedn}' --update":
                        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                        unless  => 'cat /etc/sysconfig/authconfig | egrep "^USELDAPAUTH=yes$|^USELDAP=yes$" | wc -l | egrep "^2$"',
                        before  => [ Augeas["pam-ldap-conf"],
                                     File["/etc/openldap/ldap.conf"], ],
                        require => Package["authconfig", "nss_ldap"],
                    }
                    augeas { "pam-ldap-conf":
                        context  => "/files/etc/ldap.conf",
                        changes  => [ "set nss_paged_results yes",
                                      "set pam_password exop",
                                      "set ssl ${ssl}", ],
                        notify   => Service["nscd"],
                    }
                }
            }
        }
        "Ubuntu": {
            package { [ "libpam-ldapd", "libnss-ldapd", "auth-client-config", ]:
                ensure  => installed,
            }
            exec { "auth-client-config -t nss -p ldap_example":
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                unless  => "auth-client-config -t nss -p ldap_example -s",
                require => Package["auth-client-config"],
                before  => File["/etc/nslcd.conf"],
            }
            file { "/etc/nslcd.conf":
                ensure  => present,
                content => template("ldap/nslcd.conf.erb"),
                mode    => "0640",
                owner   => "root",
                group   => "nslcd",
                require => Package["libnss-ldapd"],
                notify  => Service["nslcd"],
            }
            if $auth == "gssapi" { 
                augeas { "ldap-auth-set-principal":
                    context => "/files/etc/default/nslcd",
                    changes => "set K5START_PRINCIPAL \"\'${principal}\'\"",
                    notify  => Service["nslcd"],
                }
            }
            service { "nslcd":
                ensure => running,
                enable => true,
                before => Class["nscd"],
            }
        }
        "OpenBSD": {
            if ! $ldap_login_umask {
                $ldap_login_umask = "077"
            }
            package { "login_ldap":
                ensure => installed,
            }
            file { "/etc/login.conf":
                ensure  => present,
                content => template("ldap/login.conf.erb"),
                mode    => "0644",
                owner   => root,
                group   => wheel,
                require => [ File["/etc/openldap/ldap.conf"],
                             Package["login_ldap"], ]
            }
        }
        default: {
            fail("ldap::auth not supported on ${::operatingsystem}")
        }
    }

}

# Install and configure ldap client
#
# === Global variables
#
#   $ldap_server:
#       Array containing LDAP server URI's.
#
#   $ldap_basedn:
#       LDAP base DN.
#
class ldap::client {

    package { "openldap-client":
        name   => $::operatingsystem ? {
            "debian"  => "ldap-utils",
            "ubuntu"  => "ldap-utils",
            "openbsd" => "openldap-client",
            default   => "openldap-clients",
        },
        ensure => $::operatingsystem ? {
            darwin  => absent,
            default => installed,
        },
    }

    file { "/etc/openldap/ldap.conf":
        ensure  => present,
        content => template("ldap/ldap.conf.erb"),
        path    => $::operatingsystem ? {
            "debian" => "/etc/ldap/ldap.conf",
            "ubuntu" => "/etc/ldap/ldap.conf",
            default  => "/etc/openldap/ldap.conf",
        },
        mode    => "0644",
        owner   => root,
        group   => $::operatingsystem ? {
            "darwin"  => wheel,
            "openbsd" => wheel,
            default   => root,
        },
        require => Package["openldap-client"],
    }

}


# Install python ldap bindings.
#
class ldap::client::python {

    package { "python-ldap":
        ensure => installed,
        name   => $::operatingsystem ? {
            openbsd => "py-ldap",
            default => "python-ldap",
        },
    }

    file { "${::pythonsitedir}/dynldap.py":
        ensure  => present,
        source  => "puppet:///modules/ldap/dynldap.py",
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["python-ldap"],
    }
    python::compile { "${::pythonsitedir}/dynldap.py": }

}


# Install Ruby ldap bindings.
#
class ldap::client::ruby {

    case $::operatingsystem {
        "ubuntu","debian": {
            $pkgname = regsubst($rubyversion, '^([0-9]+\.[0-9]+)\..*', 'libldap-ruby\1')
        }
        default: {
            $pkgname = "ruby-ldap"
        }
    }

    package { "ruby-ldap":
        ensure => installed,
        name   => $pkgname,
    }

}


# Install OpenLDAP server.
#
# === Global variables
#
#   $ldap_datadir:
#       Directory for LDAP databases. Defaults to /srv/ldap.
#
#   $ldap_modules:
#       List of dynamic modules to load, syncprov and ppolicy modules
#       are always loaded.
#
#   $ldap_server_key:
#       Path to SSL private key. Defaults to puppet client key.
#
#   $ldap_server_cert:
#       Path to SSL certificate. Defaults to puppet client certificate.
#
class ldap::server {

    require ssl
    include ssl::ciphersuites

    if !$ldap_server_key {
        $ldap_server_key = "${puppet_ssldir}/private_keys/${homename}.pem"
    }
    if !$ldap_server_cert {
        $ldap_server_cert = "${puppet_ssldir}/certs/${homename}.pem"
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            $user = "openldap"
            $group = "openldap"
            $package_name = "slapd"
            $service_name = "slapd"
            $config = "/etc/ldap"
            $modulepath = "/usr/lib/ldap"
            $rundir = "/var/run/slapd"
            exec { "usermod-openldap":
                path    => "/bin:/usr/bin:/sbin:/usr/sbin",
                command => "usermod -a -G ssl-cert openldap",
                unless  => "id -n -G openldap | grep '\\bssl-cert\\b'",
                require => Package["openldap-server"],
                before  => Exec["slaptest"],
            }
        }
        "fedora": {
            $user = "ldap"
            $group = "ldap"
            $package_name = "openldap-servers"
            $service_name = "slapd"
            $config = "/etc/openldap"
            $modulepath = $architecture ? {
                "x86_64" => "/usr/lib64/openldap",
                default  => "/usr/lib/openldap",
            }
            $rundir = "/var/run/openldap"
        }
        "centos","redhat": {
            $user = "ldap"
            $group = "ldap"
            $package_name = $::operatingsystemrelease ? {
                /^5/    => [ "openldap-servers", "openldap-servers-overlays" ],
                default => "openldap-servers",
            }
            $service_name = $::operatingsystemrelease ? {
                /^5/    => "ldap",
                default => "slapd",
            }
            $config = "/etc/openldap"
            $modulepath = $architecture ? {
                "x86_64" => "/usr/lib64/openldap",
                default  => "/usr/lib/openldap",
            }
            $rundir = "/var/run/openldap"
        }
        "openbsd": {
            $user = "_openldap"
            $group = "_openldap"
            $package_name = "openldap-server"
            $service_name = "slapd"
            $config = "/etc/openldap"
            $modulepath = ""
            $rundir = "/var/run/openldap"
        }
    }

    package { "openldap-server":
        ensure => installed,
        name   => $package_name,
    }

    file { "${ssl::certs}/slapd.crt":
        ensure  => present,
        source  => $ldap_server_cert,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["openldap-server"],
        notify  => Exec["slaptest"],
    }
    file { "${ssl::private}/slapd.key":
        ensure  => present,
        source  => $ldap_server_key,
        mode    => "0640",
        owner   => "root",
        group   => $group,
        require => Package["openldap-server"],
        notify  => Exec["slaptest"],
    }

    file { "slapd.conf":
        ensure  => present,
        path    => "${config}/slapd.conf",
        content => template("ldap/slapd.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => $group,
        notify  => Exec["slaptest"],
        require => Package["openldap-server"],
    }
    file { "${config}/slapd.conf.d":
        ensure  => directory,
        source  => "puppet:///modules/custom/empty",
        mode    => "0640",
        owner   => "root",
        group   => $group,
        purge   => true,
        recurse => true,
        force   => true,
        require => Package["openldap-server"],
        notify  => Exec["generate-slapd-database-config"],
    }

    case $::operatingsystem {
        "centos","redhat": {
            if versioncmp($::operatingsystemrelease, "7") > 0 {
                file { "/etc/sysconfig/slapd":
                    ensure  => present,
                    content => template("ldap/slapd.sysconfig.erb"),
                    mode    => "0644",
                    owner   => "root",
                    group   => "root",
                    notify  => Exec["slaptest"],
                    require => Package["openldap-server"],
                }
            } elsif versioncmp($::operatingsystemrelease, "6") > 0 {
                file { "/etc/sysconfig/ldap":
                    ensure  => present,
                    content => template("ldap/ldap.sysconfig.erb"),
                    mode    => "0644",
                    owner   => "root",
                    group   => "root",
                    notify  => Exec["slaptest"],
                    require => Package["openldap-server"],
                }
            }
        }
        "debian","ubuntu": {
            file { "/etc/default/slapd":
                source  => "puppet:///modules/ldap/slapd.default",
                mode    => "0644",
                owner   => "root",
                group   => "root",
                notify  => Exec["slaptest"],
                require => Package["openldap-server"],
            }
        }
    }

    exec { "slaptest":
        command     => "slaptest -f ${config}/slapd.conf -u",
        user        => $user,
        path        => "/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        refreshonly => true,
        require     => File["${config}/slapd.conf.d"],
        notify      => Service["slapd"],
    }

    service { "slapd":
        name    => $service_name,
        start   => $::operatingsystem ? {
            "openbsd" => "/usr/local/libexec/slapd -u _openldap -h ldap:///\\ ldaps:///\\ ldapi:///",
            default   => undef,
        },
        ensure  => running,
        enable  => true,
        require => Package["openldap-server"]
    }

    if $ldap_datadir {
        file { $ldap_datadir:
            ensure  => directory,
            mode    => "0700",
            owner   => $user,
            group   => $group,
            seltype => "slapd_db_t",
            require => Package["openldap-server"],
        }
        file { "/srv/ldap":
            ensure  => link,
            target  => $ldap_datadir,
            seltype => "slapd_db_t",
            require => File[$ldap_datadir],
        }
    } else {
        file { "/srv/ldap":
            ensure  => directory,
            mode    => "0700",
            owner   => $user,
            group   => $group,
            seltype => "slapd_db_t",
            require => Package["openldap-server"],
        }
    }

    selinux::manage_fcontext { "/srv/ldap(/.*)?":
        type   => "slapd_db_t",
        before => File["/srv/ldap"],
    }
    if $ldap_datadir {
        selinux::manage_fcontext { "${ldap_datadir}(/.*)?":
            type   => "slapd_db_t",
            before => File[$ldap_datadir],
        }
    }

    file { "${config}/schema":
        ensure  => directory,
        source  => "puppet:///modules/custom/empty",
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        purge   => true,
        recurse => true,
        force   => true,
        require => Package["openldap-server"],
        notify  => Exec["generate-slapd-schema-config"],
    }
    file { "${config}/slapd.conf.d/schema.conf":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => $group,
        require => Exec["generate-slapd-schema-config"],
    }
    exec { "generate-slapd-schema-config":
        command     => "find ${config}/schema/ -name [0-9][0-9]-\\*.schema -exec echo 'include {}' \\; | sort -n > ${config}/slapd.conf.d/schema.conf",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        require     => File["${config}/slapd.conf.d"],
        notify      => Exec["slaptest"],
    }
    ldap::server::schema { [ "core", "cosine", "ppolicy", ]:
        idx => 10,
    }

    file { "${config}/slapd.conf.d/database.conf":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => $group,
        require => Exec["generate-slapd-database-config"],
        notify  => Exec["slaptest"],
    }
    exec { "generate-slapd-database-config":
        command     => "find ${config}/slapd.conf.d/db.*.conf -exec echo 'include {}' \\; > ${config}/slapd.conf.d/database.conf",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        notify      => Exec["slaptest"],
    }

}


# Create new LDAP database.
#
# === Parameters
#
#   $name:
#       Database suffix (base DN).
#
#   $aclsource:
#       Source file for custom ACL's. Default is to use template.
#
#   $master:
#       Master LDAP server URI when creating slave database.
#
#   $syncpw:
#       Password for uid=replicator,cn=config,${name} user on master.
#       Only needed for slave databases.
#
#   $rid:
#       Replica ID. Must be unique per replica per database.
#
#   $moduleoptions:
#       Options for overlay modules.
#
# === Sample usage
#
# ldap::server::database { "dc=example,dc=com":
#     moduleoptions => [ "smbkrb5pwd-enable=samba", ]
# }
#
define ldap::server::database($aclsource = "", $master = "", $syncpw = "", $rid = "", $moduleoptions = []) {

    include ldap::server

    if $rid == "" {
        $rid_real = fqdn_rand(999)
    } else {
        $rid_real = $rid
    }

    file { "${ldap::server::config}/slapd.conf.d/db.${name}.conf":
        ensure  => present,
        content => template("ldap/slapd-database.conf.erb"),
        mode    => "0640",
        owner   => "root",
        group   => $ldap::server::group,
        notify  => Exec["generate-slapd-database-config"],
    }

    file { "${ldap::server::config}/slapd.conf.d/acl.${name}.conf":
        ensure  => present,
        source  => $aclsource ? {
            ""      => undef,
            default => $aclsource,
        },
        content => $aclsource ? {
            ""      => template("ldap/slapd-acl.conf.erb"),
            default => undef,
        },
        mode    => "0640",
        owner   => "root",
        group   => $ldap::server::group,
        notify  => Exec["slaptest"],
    }

    file { "${ldap::server::config}/slapd.conf.d/index.${name}.conf":
        ensure  => present,
        source  => [ "puppet:///files/ldap/slapd-index.conf.${name}",
                     "puppet:///files/ldap/slapd-index.conf",
                     "puppet:///modules/ldap/slapd-index.conf", ],
        mode    => "0640",
        owner   => "root",
        group   => $ldap::server::group,
        notify  => Exec["slaptest"],
    }

    file { "/srv/ldap/${name}":
        ensure  => directory,
        mode    => "0700",
        owner   => $ldap::server::user,
        group   => $ldap::server::group,
        seltype => "slapd_db_t",
        require => File["/srv/ldap"],
    }

    file { "/srv/ldap/${name}/DB_CONFIG":
        ensure  => present,
        source  => [ "puppet:///files/ldap/DB_CONFIG.${name}",
                     "puppet:///files/ldap/DB_CONFIG",
                     "puppet:///modules/ldap/DB_CONFIG", ],
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        seltype => "slapd_db_t",
        require => File["/srv/ldap/${name}"],
        before  => Exec["slaptest"],
    }

}


# Install LDAP daily backup job
#
# === Parameters
#
#   $datadir:
#       Directory where LDAP backups are stored. Defaults to /srv/ldap-backup
#
#   $maxage:
#       How long to keep LDAP backups. Defaults to 168 hours (7 days).
#
class ldap::server::backup($datadir="/srv/ldap-backup", $maxage="168") {

    if $datadir != "/srv/ldap-backup" {
        file { "/srv/ldap-backup":
            ensure  => link,
            target  => $datadir,
            owner   => "root",
            group   => "root",
            require => File[$datadir],
        }
    }
    file { $datadir:
        ensure => directory,
        mode   => "0700",
        owner  => $ldap::server::user,
        group  => $ldap::server::group,
    }

    file { "/usr/local/sbin/ldap-backup.cron":
        ensure  => present,
        content => template("ldap/ldap-backup.cron.erb"),
        mode    => "0755",
        owner   => "root",
        group   => "root",
        require => File["/srv/ldap-backup"],
    }
    cron { "ldap-backup":
        ensure  => present,
        command => "/usr/local/sbin/ldap-backup.cron",
        user    => $ldap::server::user,
        hour    => "0",
        minute  => "10",
        require => File["/usr/local/sbin/ldap-backup.cron"],
    }

}


# Install custom schema to OpenLDAP.
#
# === Parameters
#
#   $name:
#       Schema name.
#
#   $idx:
#       Schema load order. Defaults to 50.
#
# === Sample usage
#
# ldap::server::schema { "samba": }
#
define ldap::server::schema($idx = 50) {

    include ldap::server

    file { "${name}.schema":
        ensure  => present,
        path    => "${ldap::server::config}/schema/${idx}-${name}.schema",
        source  => [ "puppet:///files/ldap/${name}.schema",
                     "puppet:///modules/ldap/${name}.schema", ],
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["openldap-server"],
        notify  => Exec["generate-slapd-schema-config"],
    }
}

