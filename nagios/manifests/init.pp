# Common nagios variables.
#
class nagios::common {

    case $::operatingsystem {
        "centos","redhat","fedora": {
            $etcdir = "/etc/nagios"
            $confdir = "${etcdir}/conf.d"
            $private = "${etcdir}/private.d"
            $htpasswd = "${etcdir}/passwd"
            $package = "nagios"
            $service = "nagios"
            $scriptalias = "/nagios/cgi-bin/"
            $libdir = $::architecture ? {
                "x86_64" => "/usr/lib64/nagios/plugins",
                default  => "/usr/lib/nagios/plugins",
            }
            $cgibin = $::architecture ? {
                "x86_64" => "/usr/lib64/nagios/cgi-bin",
                default  => "/usr/lib/nagios/cgi-bin",
            }
            $htdocs = "/usr/share/nagios/html"
        }
        "ubuntu","debian": {
            $etcdir = "/etc/nagios3"
            $confdir = "${etcdir}/conf.d"
            $private = "${etcdir}/private.d"
            $htpasswd = "${etcdir}/htpasswd.users"
            $package = "nagios3"
            $service = "nagios3"
            $scriptalias = "/cgi-bin/nagios3/"
            $libdir = "/usr/lib/nagios/plugins"
            $cgibin = "/usr/lib/cgi-bin/nagios3"
            $htdocs = "/usr/share/nagios3/htdocs"
        }
        "openbsd": {
            # no params set as we don't support server on openbsd yet
            $libdir = "/usr/local/libexec/nagios"
        }
        default: {
            fail("Nagios not supported on ${::operatingsystem}")
        }
    }

}


# Install Nagios server.
#
class nagios::server::manual inherits nagios::common {

    package { "nagios":
        ensure => installed,
        name   => $package,
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { [ "nagios-plugins-all",
                        "nagios-plugins-nrpe", ]:
                ensure => installed,
            }
        }
        "ubuntu","debian": {
            package { [ "nagios-plugins",
                        "nagios-nrpe-plugin", ]:
                ensure => installed,
            }
            file { "/usr/share/nagios3/htdocs/stylesheets":
                ensure => link,
                target => "/etc/nagios3/stylesheets",
            }
        }
        default: {
            fail("nagios::server not supported in ${::operatingsystem}")
        }
    }

    exec { "usermod-nagios-httpsd":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        command => "usermod -a -G nagios ${apache::sslserver::user}",
        unless  => "id -n -G ${apache::sslserver::user} | grep '\\bnagios\\b'",
        require => Package["nagios"],
    }

    service { "nagios":
        ensure    => running,
        enable    => true,
        name      => $service,
        hasstatus => true,
    }

    apache::configfile { "nagios.conf":
        content => template("nagios/nagios-httpd.conf.erb"),
    }

    file { [ "${htdocs}/.htaccess", "${cgibin}/.htaccess" ]:
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [ "puppet:///files/nagios/htaccess",
                     "puppet:///modules/nagios/htaccess.${::osfamily}", ],
        require => Package["nagios"],
    }

    file { "${htdocs}/config.inc.php":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => $apache::sslserver::group,
        require => Package["nagios"],
    }

    file { "/etc/nagios/nagios.cfg":
        ensure  => present,
        name    => "${etcdir}/nagios.cfg",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => "puppet:///modules/nagios/nagios.cfg.${::osfamily}",
        require => Package["nagios"],
        notify  => Service["nagios"],
    }

    file { "/etc/nagios/cgi.cfg":
        ensure  => present,
        name    => "${etcdir}/cgi.cfg",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("nagios/cgi.cfg.erb"),
        require => Package["nagios"],
        notify  => Service["nagios"],
    }

    file { "/etc/nagios/commands.cfg":
        ensure  => present,
        name    => "${etcdir}/commands.cfg",
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => "puppet:///modules/nagios/commands.cfg",
        require => Package["nagios"],
        notify  => Service["nagios"],
    }

    file { "/etc/nagios/passwd":
        ensure  => present,
        name    => $htpasswd,
        mode    => "0640",
        owner   => "root",
        group   => $apache::sslserver::group,
        require => Package["nagios"],
    }

    file { "/etc/nagios/conf.d":
        ensure  => directory,
        name    => $confdir,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
        require => Package["nagios"],
        notify  => Service["nagios"],
    }

    file { "/etc/nagios/private.d":
        ensure  => directory,
        name    => $private,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
        require => Package["nagios"],
        notify  => Service["nagios"],
    }

    file { "${confdir}/contactgroup_all.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_contactgroup["all"],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_contactgroup { "all":
        target => "${confdir}/contactgroup_all.cfg",
        notify => Service["nagios"],
    }

    file { "${confdir}/host_default.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_host["default"],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_host { "default":
        target                       => "${confdir}/host_default.cfg",
        register                     => "0",
        active_checks_enabled        => "1",
        passive_checks_enabled       => "1",
        notifications_enabled        => "1",
        flap_detection_enabled       => "1",
        process_perf_data            => "1",
        retain_status_information    => "1",
        retain_nonstatus_information => "1",
        check_command                => "check-host-alive",
        check_period                 => "24x7",
        check_interval               => "5",
        retry_interval               => "1",
        max_check_attempts           => "3",
        notification_period          => "24x7",
        notification_interval        => "0",
        notification_options         => "d,u,r,f,s",
        contact_groups               => "all",
        notify                       => Service["nagios"],
    }

    file { "${confdir}/service_default.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_service["default"],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_service { "default":
        target                       => "${confdir}/service_default.cfg",
        register                     => "0",
        active_checks_enabled        => "1",
        passive_checks_enabled       => "1",
        notifications_enabled        => "1",
        flap_detection_enabled       => "1",
        process_perf_data            => "1",
        retain_status_information    => "1",
        retain_nonstatus_information => "1",
        check_period                 => "24x7",
        check_interval               => "5",
        retry_check_interval         => "1",
        max_check_attempts           => "3",
        notification_period          => "24x7",
        notification_interval        => "0",
        notification_options         => "w,u,c,r,f,s",
        contact_groups               => "all",
        notify                       => Service["nagios"],
    }

    file { "${confdir}/timeperiod_24x7.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_timeperiod["24x7"],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_timeperiod { "24x7":
        target    => "${confdir}/timeperiod_24x7.cfg",
        alias     => "24x7",
        monday    => "00:00-24:00",
        tuesday   => "00:00-24:00",
        wednesday => "00:00-24:00",
        thursday  => "00:00-24:00",
        friday    => "00:00-24:00",
        saturday  => "00:00-24:00",
        sunday    => "00:00-24:00",
        notify    => Service["nagios"],
    }

    require samba::client
    file { "${nagios::common::libdir}/check_wins":
        ensure  => present,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        source  => "puppet:///modules/nagios/check_wins",
        require => Package["nagios"],
        before  => Service["nagios"],
    }
    file { "${nagios::common::libdir}/check_smb":
        ensure  => present,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        source  => "puppet:///modules/nagios/check_smb",
        require => Package["nagios"],
        before  => Service["nagios"],
    }
    file { "${nagios::common::libdir}/check_tftp":
        ensure  => present,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        source  => "puppet:///modules/nagios/check_tftp",
        require => Package["nagios"],
        before  => Service["nagios"],
    }

}


# Install Nagios server and collect node configurations.
#
class nagios::server inherits nagios::server::manual {

    Nagios::Host <<||>> {
        confdir => $confdir,
    }
    Nagios::Service <<||>> {
        confdir => $confdir,
    }

}


# Install nagios NSCA service
#
class nagios::server::nsca {

    package { "nsca":
        ensure => installed,
    }

    service { "nsca":
        ensure => running,
        enable => true,
    }

}


# Define nagios email contact.
#
# === Parameters
#
#   $name:
#       Email address.
#   $group:
#       List of contact groups. Defaults to "all".
#
define nagios::contact::email($group=["all"],
                              $confdir=$nagios::common::confdir) {

    $contactgroups = inline_template('<%= @group.join(",") -%>')

    file { "${confdir}/contact_${name}.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_contact[$name],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_contact { $name:
        target                        => "${confdir}/contact_${name}.cfg",
        contactgroups                 => $contactgroups,
        host_notification_commands    => "notify-host-by-email",
        host_notification_options     => "d,u,r,s",
        host_notification_period      => "24x7",
        service_notification_commands => "notify-service-by-email",
        service_notification_options  => "w,u,c,r,s",
        service_notification_period   => "24x7",
        email                         => $name,
        notify                        => Service["nagios"],
    }

}


# Define nagios prowl contact.
#
# === Parameters
#
#   $name:
#       Descriptive name for contact.
#   $key:
#       Prowl API key.
#   $group:
#       List of contact groups. Defaults to "all".
#
define nagios::contact::prowl($key, $group=["all"],
                              $confdir=$nagios::common::confdir) {

    $contactgroups = inline_template('<%= @group.join(",") -%>')

    file { "${confdir}/contact_prowl_${name}.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_contact["prowl-${name}"],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_contact { "prowl-${name}":
        target                        => "${confdir}/contact_prowl_${name}.cfg",
        contactgroups                 => $contactgroups,
        host_notification_commands    => "notify-host-by-prowl",
        host_notification_options     => "d,u",
        host_notification_period      => "24x7",
        service_notification_commands => "notify-service-by-prowl",
        service_notification_options  => "c",
        service_notification_period   => "24x7",
        address1                      => $key,
        notify                        => Service["nagios"],
    }

}


# Define nagios pushover contact.
#
# === Parameters
#
#   $name:
#       Descriptive name for contact.
#   $key:
#       Pushover user key.
#   $token:
#       Pushover application token.
#   $group:
#       List of contact groups. Defaults to "all".
#
define nagios::contact::pushover($key, $token, $group=["all"],
                                 $confdir=$nagios::common::confdir) {

    $contactgroups = inline_template('<%= @group.join(",") -%>')

    file { "${confdir}/contact_pushover_${name}.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_contact["pushover-${name}"],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_contact { "pushover-${name}":
        target                        => "${confdir}/contact_pushover_${name}.cfg",
        contactgroups                 => $contactgroups,
        host_notification_commands    => "notify-host-by-pushover",
        host_notification_options     => "d,u",
        host_notification_period      => "24x7",
        service_notification_commands => "notify-service-by-pushover",
        service_notification_options  => "c",
        service_notification_period   => "24x7",
        address1                      => $key,
        address2                      => $token,
        notify                        => Service["nagios"],
    }

}


# Define nagios sms contact.
#
# This requires gammu::smsd running on same host.
#
# === Parameters
#
#   $name:
#       Descriptive name for contact.
#   $number:
#       Phone number where to send messages.
#   $group:
#       List of contact groups. Defaults to "all".
#
define nagios::contact::sms($number, $group=["all"],
                            $confdir=$nagios::common::confdir) {

    $contactgroups = inline_template('<%= @group.join(",") -%>')

    file { "${confdir}/contact_sms_${name}.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_contact["sms-${name}"],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_contact { "sms-${name}":
        target                        => "${confdir}/contact_sms_${name}.cfg",
        contactgroups                 => $contactgroups,
        host_notification_commands    => "notify-host-by-sms",
        host_notification_options     => "d,u",
        host_notification_period      => "24x7",
        service_notification_commands => "notify-service-by-sms",
        service_notification_options  => "c",
        service_notification_period   => "24x7",
        pager                         => $number,
        notify                        => Service["nagios"],
    }

}


# Define nagios target host.
#
# === Parameters
#
#   $name:
#       Hostname of target.
#   $address:
#       IP address of target. Default is to use hostname.
#   $group:
#       Hostgroup name.
#   $osname:
#       Operating system name.
#   $osicon:
#       Operating system icon name.
#   $parent:
#       Parent hostname.
#
define nagios::host($address=undef, $group="NONE", $osname="NONE",
                    $osicon="NONE", $confdir=$nagios::common::confdir,
                    $parent=undef) {

    file { "${confdir}/host_${name}.cfg":
        ensure  => present,
                mode    => "0640",
                owner   => "root",
                group   => "nagios",
                before  => Nagios_host[$name],
                require => File["/etc/nagios/conf.d"],
    }
    nagios_host { $name:
        ensure  => present,
        address => $address,
        use     => "default",
        target  => "${confdir}/host_${name}.cfg",
        parents => is_array($parent) ? {
            true  => inline_template('<%= @parent.join(",") -%>'),
            false => $parent,
        },
        notify  => Service["nagios"],
    }

    if $osicon != "NONE" {
        case $::operatingsystem {
            "debian","ubuntu": {
                $iconpath = "base/"
            }
            default: {
                $iconpath = ""
            }
        }
        Nagios_host[$name] {
            icon_image_alt  => $osname,
            icon_image      => "${iconpath}${osicon}.png",
            statusmap_image => "${iconpath}${osicon}.gd2",
        }
    }

    if $group != "NONE" {
        if !defined(Nagios_contactgroup[$group]) {
            file { "${confdir}/contactgroup_${group}.cfg":
                ensure  => present,
                mode    => "0640",
                owner   => "root",
                group   => "nagios",
                before  => Nagios_contactgroup[$group],
                require => File["/etc/nagios/conf.d"],
            }
            nagios_contactgroup { $group:
                target => "${confdir}/contactgroup_${group}.cfg",
                notify => Service["nagios"],
            }
        }
        if !defined(Nagios_hostgroup[$group]) {
            file { "${confdir}/hostgroup_${group}.cfg":
                ensure  => present,
                mode    => "0640",
                owner   => "root",
                group   => "nagios",
                before  => Nagios_hostgroup[$group],
                require => File["/etc/nagios/conf.d"],
            }
            nagios_hostgroup { $group:
                target => "${confdir}/hostgroup_${group}.cfg",
                notify => Service["nagios"],
            }
        }
        Nagios_host[$name] {
            contact_groups => "all,${group}",
            hostgroups     => $group,
        }
    }

}


# Define nagios target service.
#
# === Parameters
#
#   $host:
#       Target hostname.
#   $command:
#       Check command.
#   $description:
#       Service description.
#   $group:
#       Hostgroup name.
#   $depended_host:
#       Hostname of optional service dependency.
#   $depended_service
#       Description of optional service dependency.
#
define nagios::service($host, $command, $description, $group="NONE",
                       $depended_host=undef, $depended_service=undef,
                       $confdir=$nagios::common::confdir) {

    file { "${confdir}/service_${name}.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        before  => Nagios_service[$name],
        require => File["/etc/nagios/conf.d"],
    }
    nagios_service { $name:
        host_name           => $host,
        check_command       => $command,
        service_description => $description,
        use                 => "default",
        target              => "${confdir}/service_${name}.cfg",
        require             => Nagios_host[$host],
        notify              => Service["nagios"],
    }

    if $group != "NONE" {
        if !defined(Nagios_servicegroup[$group]) {
            file { "${confdir}/servicegroup_${group}.cfg":
                ensure  => present,
                mode    => "0640",
                owner   => "root",
                group   => "nagios",
                before  => Nagios_servicegroup[$group],
                require => File["/etc/nagios/conf.d"],
            }
            nagios_servicegroup { $group:
                target => "${confdir}/servicegroup_${group}.cfg",
                notify => Service["nagios"],
            }
        }
        Nagios_service[$name] {
            contact_groups => "all,${group}",
            servicegroups  => $group,
        }
    }
    if $depended_host and $depended_service {
        file { "${confdir}/service_dependency_${name}.cfg":
            ensure  => present,
            mode    => "0640",
            owner   => "root",
            group   => "nagios",
            before  => Nagios_servicedependency[$name],
            require => File["/etc/nagios/conf.d"],
        }
        nagios_servicedependency { $name:
            dependent_host_name           => $host,
            dependent_service_description => $description,
            host_name                     => $depended_host,
            service_description           => $depended_service,
            execution_failure_criteria    => 'c,p',
            target                        => "${confdir}/service_dependency_${name}.cfg",
            notify                        => Service["nagios"],
        }
    }

}


# Install check_esxi_hardware.py plugin.
#
class nagios::plugin::esxi {

    package { "pywbem":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "python-pywbem",
            "ubuntu" => "python-pywbem",
            default  => "pywbem",
        },
     }

     file { "${nagios::common::libdir}/check_esxi_hardware.py":
        ensure  => present,
        mode    => "0755",
        owner   => "root",
        group   => "root",
        source  => "puppet:///files/nagios/check_esxi_hardware.py",
        require => Package["nagios"],
    }

}


# Define ESXi target host.
#
# === Parameters
#
#   $name:
#       Target hostname.
#   $user:
#       ESXi host username.
#   $pass:
#       ESXi host password.
#   $vendor:
#       ESXi host vendor. Defaults to "auto".
#
define nagios::host::esxi($user, $pass, $vendor="auto") {

    file { "${nagios::common::private}/esxi_${name}.cfg":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => "nagios",
        content => "${user} ${pass}\n",
        require => File["/etc/nagios/private.d"],
    }

    nagios::host { $name:
        confdir => $nagios::common::confdir,
        notify  => Service["nagios"],
    }

    nagios::service { "${name}_esxi":
        host        => $name,
        command     => "check_esxi_hardware!${nagios::common::private}/esxi_${name}.cfg!${vendor}",
        description => "ESXi",
        confdir     => $nagios::common::confdir,
        notify      => Service["nagios"],
    }

}


# Define HP ProCurve target host.
#
#   $name:
#       Target hostname.
#   $community:
#       SNMP community.
#   $group:
#       Hostgroup.
#
define nagios::host::procurve($community, $group="NONE") {

    include snmp::utils

    # 4 = good, 3 = warning, 2 = bad
    $warn = "4:,4:,4:"
    $crit = "3:,3:,3:"
    # Fan, PSU1, PSU2
    $oids = ".1.3.6.1.4.1.11.2.14.11.1.2.6.1.4.1,.1.3.6.1.4.1.11.2.14.11.1.2.6.1.4.2,.1.3.6.1.4.1.11.2.14.11.1.2.6.1.4.3"

    nagios::host { $name:
        group  => $group,
        osname => "ProCurve",
        osicon => "switch",
    }
    nagios::service { "${name}_check_snmp":
        host        => $name,
        group       => $group,
        command     => "check_snmp!-l FAN,PSU1,PSU2 -C ${community} -w ${warn} -c ${crit} -o ${oids}",
        description => "Sensors",
        require     => Package["net-snmp-utils"],
    }

}
