# Configure nagios target.
#
# === Global variables
#
#   $nagios_target_group:
#       Host and service group name. Defaults to $domain.
#
#   $nagios_target_parent:
#       Parent hostname.
#
class nagios::target {

    if $nagios_target_group {
        $group = $nagios_target_group
    } else {
        if $::domain {
            $group = $::domain
        } else {
            $group = "unknown"
        }
    }

    if $nagios_target_parent {
        $parent = $nagios_target_parent
    } elsif $::libvirt_host {
        $parent = $::libvirt_host
    } else {
        $parent = undef
    }

    @@nagios::host { $::homename:
        group  => $group,
        osname => $::operatingsystem,
        osicon => $::osfamily ? {
            ""      => "NONE",
            default => inline_template("<%= @osfamily.downcase %>")
        },
        parent => $parent,
    }

    Nagios::Service {
        host  => $::homename,
        group => $group,
    }

}


# Configure dhcp service target.
#
class nagios::target::dhcp inherits nagios::target {

    @@nagios::service { "${::homename}_dhcp":
        command     => "check_dhcp!${::homename}",
        description => "DHCP",
    }

}


# Configure dig service target.
#
# === Parameters
#
#     $query:
#         Address to query from DNS server. Defaults to localhost.
#
class nagios::target::dns ($query="localhost") inherits nagios::target {

    @@nagios::service { "${::homename}_dig":
        command     => "check_dig!${query}",
        description => "DNS",
    }

}


# Configure ssh service target.
#
class nagios::target::ssh inherits nagios::target {

    @@nagios::service { "${::homename}_ssh":
        command     => "check_ssh",
        description => "SSH",
    }

}


# Configure http service target.
#
class nagios::target::http inherits nagios::target {

    @@nagios::service { "${::homename}_http":
        command     => "check_http",
        description => "HTTP",
    }

}


# Configure https service target.
#
# === Parameters
#
#   $expect:
#       Comma-delimited list of strings, at least one of them is
#       expected in the first (status) line of the server response
#       (default: HTTP/1.) If specified skips all other status line
#       logic (ex: 3xx, 4xx, 5xx processing)
#
class nagios::target::https($expect=undef) inherits nagios::target {

    @@nagios::service { "${::homename}_https":
        command     => $expect ? {
            undef   => "check_http!--ssl",
            default => "check_http!--ssl --expect='${expect}'",
        },
        description => "HTTPS",
    }

    @@nagios::service { "${::homename}_https_certificate":
        command          => "check_http!--ssl -C 14",
        description      => "HTTPS Certificate",
        depended_host    => $::homename,
        depended_service => "HTTPS",
    }

}


# Configure imaps service target.
#
class nagios::target::imaps inherits nagios::target {

    @@nagios::service { "${::homename}_imaps":
        command     => "check_imap!--ssl -p 993",
        description => "IMAPS",
    }

    @@nagios::service { "${::homename}_imaps_certificate":
        command          => "check_imap!--ssl -p 993 -D 14",
        description      => "IMAPS Certificate",
        depended_host    => $::homename,
        depended_service => "IMAPS",
    }

}


# Configure ipp service target.
#
class nagios::target::ipp inherits nagios::target {

    @@nagios::service { "${::homename}_ipp":
        command     => "check_http!-p 631",
        description => "IPP",
    }

}


# Configure ipp with SSL service target.
#
class nagios::target::ipp::ssl inherits nagios::target {

    @@nagios::service { "${::homename}_ipp_ssl":
        command     => "check_http!-p 631 --ssl",
        description => "IPP + SSL",
    }

    @@nagios::service { "${::homename}_ipp_ssl_certificate":
        command          => "check_http!-p 631 --ssl -C 14",
        description      => "IPP + SSL Certificate",
        depended_host    => $::homename,
        depended_service => "IPP + SSL",
    }

}


# Configure jabber service target.
#
class nagios::target::jabber inherits nagios::target {

    @@nagios::service { "${::homename}_jabber":
        command     => "check_jabber!--ssl -p 5223",
        description => "Jabber",
    }

    @@nagios::service { "${::homename}_jabber_certificate":
        command          => "check_jabber!--ssl -p 5223 -D 14",
        description      => "Jabber Certificate",
        depended_host    => $::homename,
        depended_service => "Jabber",
    }

}


# Configure ldap service target.
#
class nagios::target::ldap inherits nagios::target {

    @@nagios::service { "${::homename}_ldap":
        command     => "check_ldap!${ldap_basedn}",
        description => "LDAP",
    }

}


# Configure ldaps service target.
#
class nagios::target::ldaps inherits nagios::target {

    @@nagios::service { "${::homename}_ldaps":
        command     => "check_ldap!${ldap_basedn} -p 636",
        description => "LDAPs",
    }

}


# Configure lpd service target.
#
class nagios::target::lpd inherits nagios::target {

    @@nagios::service { "${::homename}_lpd":
        command     => "check_tcp!515",
        description => "LPD",
    }

}


# Configure munin service target.
#
class nagios::target::munin inherits nagios::target {

    @@nagios::service { "${::homename}_munin":
        command     => "check_tcp!4949 -e 'munin node at ' -q quit",
        description => "Munin",
    }

}


# Configure pop3 service target.
#
class nagios::target::pop3s inherits nagios::target {

    @@nagios::service { "${::homename}_pop3s":
        command     => "check_pop!--ssl -p 995",
        description => "POP3s",
    }

    @@nagios::service { "${::homename}_pop3s_certificate":
        command          => "check_pop!--ssl -p 995 -D 14",
        description      => "POP3s Certificate",
        depended_host    => $::homename,
        depended_service => "POP3s",
    }

}


# Configure puppet service target.
#
class nagios::target::puppet inherits nagios::target {

    @@nagios::service { "${::homename}_puppet":
        command     => "check_http!--ssl -p 8140 -e 'HTTP/1.1 400'",
        description => "Puppet Master",
    }

    @@nagios::service { "${::homename}_puppet_certificate":
        command          => "check_http!--ssl -p 8140 -C 30,14",
        description      => "Puppet Certificate",
        depended_host    => $::homename,
        depended_service => "Puppet Master",
    }

}


# Configure SMB service target.
#
class nagios::target::smb inherits nagios::target {

    @@nagios::service { "${::homename}_smb":
        command     => "check_smb",
        description => "SMB",
    }

}


# Configure smtp service target.
#
class nagios::target::smtp inherits nagios::target {

    @@nagios::service { "${::homename}_smtp":
        command     => "check_smtp",
        description => "SMTP",
    }

}


# Configure submission service target.
#
class nagios::target::submission inherits nagios::target {

    @@nagios::service { "${::homename}_submission":
        command     => "check_smtp!--starttls -p 587",
        description => "Submission",
    }

    @@nagios::service { "${::homename}_submission_certificate":
        command          => "check_smtp!--starttls -p 587 -D 14",
        description      => "Submission Certificate",
        depended_host    => $::homename,
        depended_service => "Submission",
    }

}


# Configure tftp service target.
#
# === Parameters
#
#     $file:
#         Filename to fetch from TFTP server
#
class nagios::target::tftp($file) inherits nagios::target {

    @@nagios::service { "${::homename}_tftp":
        command     => "check_tftp!${file}",
        description => "TFTP",
    }

}


# Configure tcp connect service target.
#
# === Parameters
#
#     $name:
#         Short name
#     $port:
#         Port where to connect
#     $description:
#         Description of service. Defaults to $name
#
# === Sample usage
#
# nagios::target::tcp { "git":
#     port        => "9418",
#     description => "GIT",
# }
#
define nagios::target::tcp($port, $description=undef) {

    include nagios::target

    if ! $description {
        $description = $name
    }

    @@nagios::service { "${::homename}_${name}":
        command     => "check_tcp!${port}",
        description => $description,
        group       => $nagios::target::group,
        host        => $::homename,
    }

}


# Configure WINS service target.
#
# === Parameters:
#
#     $query:
#         Address to query from WINS. Defaults to $homename.
#
#     $type:
#         Record type to query. Defaults to 00 (workstation service).
#         For different types see: http://support.microsoft.com/kb/830578
#
class nagios::target::wins($query="${::homename}", $type="00") inherits nagios::target {

    @@nagios::service { "${::homename}_wins":
        command     => "check_wins!${query} -t ${type}",
        description => "WINS",
    }

}


# Configure nagios nrpe target.
#
#   === Global variables
#
#   $nagios_allow:
#       Array of IP addresses allowed to connect to the nrpe service.
#
class nagios::target::nrpe inherits nagios::target {

    if !$nagios_allow {
        $nagios_allow = [ "127.0.0.1" ]
    }

    include nagios::common

    case $operatingsystem {
        "centos","redhat","fedora": {
            $package = "nrpe"
            $service = "nrpe"
            $nrpecfg = "/etc/nagios/nrpe.cfg"
            $nrpedir = "/etc/nrpe.d"
            $nrpepid = "/var/run/nrpe/nrpe.pid"
            $nrpeuser = "nrpe"
            $nrpegroup = "nrpe"
        }
        "ubuntu","debian": {
            $package = "nagios-nrpe-server"
            $service = "nagios-nrpe-server"
            $nrpecfg = "/etc/nagios/nrpe.cfg"
            $nrpedir = "/etc/nagios/nrpe.d"
            $nrpepid = "/var/run/nagios/nrpe.pid"
            $nrpeuser = "nagios"
            $nrpegroup = "nagios"
        }
        "openbsd": {
            $package = "nrpe"
            $service = "nrpe"
            $nrpecfg = "/etc/nrpe.cfg"
            $nrpedir = "/etc/nrpe.d"
            $nrpepid = "/var/run/nrpe.pid"
            $nrpeuser = "_nrpe"
            $nrpegroup = "_nrpe"
        }
    }

    package { "nrpe":
        ensure => installed,
        name   => $package,
    }

    file { "/etc/nrpe.cfg":
        ensure  => present,
        name    => $nrpecfg,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => template("nagios/nrpe.cfg.erb"),
        require => Package["nrpe"],
        notify  => Service["nrpe"],
    }

    file { "/etc/nrpe.d":
        ensure  => directory,
        name    => $nrpedir,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        purge   => true,
        force   => true,
        recurse => true,
        source  => "puppet:///modules/custom/empty",
        require => Package["nrpe"],
    }

    service { "nrpe":
        ensure => running,
        enable => true,
        name   => $service,
    }

    nagios::target::nrpe::service { "check_disk -w 10% -c 5% -p /":
        description => "Disk",
        package     => $::operatingsystem ? {
            "openbsd" => undef,
            "debian"  => "nagios-plugins-basic",
            "ubuntu"  => "nagios-plugins-basic",
            default   => "nagios-plugins-disk",
        }
    }
    nagios::target::nrpe::service { "check_load -r -w 3,2,1 -c 6,4,2":
        description => "Load",
        package     => $::operatingsystem ? {
            "openbsd" => undef,
            "debian"  => "nagios-plugins-basic",
            "ubuntu"  => "nagios-plugins-basic",
            default   => "nagios-plugins-load",
        }
    }
    if regsubst($::swapsize, '^([0-9]+)\..*', '\1') != 0 {
        nagios::target::nrpe::service { "check_swap -w 75% -c 50%":
            description => "Swap",
            package     => $::operatingsystem ? {
                "openbsd" => undef,
                "debian"  => "nagios-plugins-basic",
                "ubuntu"  => "nagios-plugins-basic",
                default   => "nagios-plugins-swap",
            }
        }
    }

}


# Add new nagios nrpe service check
#
# === Parameters
#
#   $name:
#       Symbolic name for service check.
#   $command:
#       Check command (including arguments). Defaults to $name.
#   $description:
#       Service description. Defaults to command name without
#       check_ prefix.
#   $package:
#       Package providing check command.
#   $source:
#       Source file for check command.
#
# === Example usage
#
# nagios::target::nrpe::service { "check_disk -w 20% -c 10% -p /":
#     description => "Disk",
#     package     => $::operatingsystem ? {
#         "openbsd" => undef,
#         "debian"  => "nagios-plugins-basic",
#         "ubuntu"  => "nagios-plugins-basic",
#         default   => "nagios-plugins-disk",
#     }
# }
#
define nagios::target::nrpe::service($command=undef,
                                     $source=undef,
                                     $description=undef,
                                     $package=undef) {

    include nagios::target::nrpe

    if $command {
        $command_real = $command
        $binary = regsubst($command_real, '^([^ ]+) .*', '\1')
        $config = $name
    } else {
        $command_real = $name
        $binary = regsubst($command_real, '^([^ ]+) .*', '\1')
        $config = $binary
    }

    if !$description {
        $description = regsubst($binary, '^check_(.+)', '\1')
    }

    if $source and !defined(File["${nagios::common::libdir}/${binary}"]) {
        file { "${nagios::common::libdir}/${binary}":
            ensure  => present,
            source  => $source,
            mode    => "0755",
            owner   => "root",
            group   => $::operatingsystem ? {
                "openbsd" => "wheel",
                default   => "root",
            },
            require => Package["nrpe"],
            notify  => Service["nrpe"],
        }
    }

    if $package and !defined(Package[$package]) {
        package { $package:
            ensure  => present,
            require => Package["nrpe"],
            before  => Service["nrpe"],
        }
    }

    file { "${nagios::target::nrpe::nrpedir}/${config}.cfg":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => "command[${config}]=${nagios::common::libdir}/${command_real}\n",
        require => File["/etc/nrpe.d"],
        notify  => Service["nrpe"],
    }

    @@nagios::service { "${::homename}_${config}":
        command     => "check_nrpe!${config}",
        description => $description,
        host        => $::homename,
    }

}


# Install NSCA client tools
#
class nagios::target::nsca inherits nagios::target {

    package { "nsca-client":
        ensure => installed,
    }

}


# Register new NSCA service to Nagios server
#
# === Sample usage:
#
#     nagios::target::nsca::service { "test": }
#
define nagios::target::nsca::service() {

    require nagios::target::nsca

    @@nagios::service { "${::homename}_${name}":
        command     => "check_dummy!0",
        description => $name,
        host        => $::homename,
    }

}
