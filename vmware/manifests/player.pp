
# Install VMware Player
#
# Requires following files on puppet server:
#
#   /srv/puppet/files/common/packages/VMware-Player.i386.bundle
#   /srv/puppet/files/common/packages/VMware-Player.x86_64.bundle
#
class vmware::player {

    vmware::bundle { "VMware-Player": }

}
