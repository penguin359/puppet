
require 'base64'
require 'expect'
require 'tempfile'


module Puppet::Parser::Functions
  newfunction(:keytab_generate) do |args|
      name = args[0]
      principals = args[1]

      # get output file name
      outfile = File.join('/srv/puppet/files/generated',
                          lookupvar('homename'), Base64.encode64(name)).strip
      begin
        Dir.mkdir(File.dirname(outfile))
      rescue
        nil
      end

      # check if we have cached keytab up to date
      cached = true
      if File.exists?(outfile)
        if not check_keytab(outfile, principals)
          cached = false
          File.unlink(outfile)
        end
      else
        cached = false
      end

      # create new keytab if cache is not up to date
      if not cached
        cmd = sprintf('kadmin -p %s -k -t /etc/puppet/puppet.keytab -q "ktadd -k %s %s" 1>&2',
                      lookupvar('kerberos_user'), outfile, principals.join(' '))
        output = ''
        IO.popen(cmd, mode='r') { |f|
          output = f.read
        }
        if not File.exists?(outfile)
          raise 'Failed to create keytab ' + name + ' error was: ' + output
        elsif not check_keytab(outfile, principals)
          raise 'Invalid keytab ' + name + ' created'
        end
      end
  end

end


# function to check if keytab contains required principals
def check_keytab(keytab, principals)
  entries = []
  IO.popen(sprintf('klist -k %s', keytab), mode='r') { |f|
    f.readlines.each do |l|
      next unless l =~ /[ ]+\d+ .*/
      entries << l.split()[1]
    end
  }
  principals.each do |p|
    if not entries.include?(p)
      return false
    end
  end
  return true
end
