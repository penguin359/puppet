
# Add client into BackupPC server
#
# === Parameters
#
#   $name:
#       Client hostname
#
#   $ensure:
#       If set to present client will be added and if absent
#       it will be removed from server.
#
#   $operatingsystem:
#       Operatingsystem of client. Used to find correct client
#       configuration. Defaults to "default".
#
define backuppc::manualclient($ensure = "present", $operatingsystem = "default") {

    @@file { "/etc/BackupPC/pc/${name}.pl":
        ensure  => $ensure,
        source  => [ "puppet:///files/backuppc/${name}.pl",
                     "puppet:///files/backuppc/${operatingsystem}.pl",
                     "puppet:///files/backuppc/default.pl",
                     "puppet:///modules/backuppc/default.pl", ],
        mode    => "0640",
        owner   => root,
        group   => backuppc,
        tag     => "backuppc",
        require => File["/etc/BackupPC/pc"],
        notify  => Exec["generate-backuppc-hosts"],
    }

}

# Install host into BackupPC server as client.
#
class backuppc::client {

    backuppc::manualclient { $homename:
        ensure          => present,
        operatingsystem => $::operatingsystem,
    }

    include rsync

    Ssh_authorized_key <<| tag == "backuppc" |>>

}

# Install BackupPC server and add defined clients.
#
class backuppc::server {

    include rsync

    include user::system
    realize(User["backuppc"], Group["backuppc"])

    package { "BackupPC":
        ensure  => installed,
        require => [ User["backuppc"],
                     Group["backuppc"], ],
    }

    if $backuppc_datadir {
        file { [
           $backuppc_datadir,
           "${backuppc_datadir}/trash",
           "${backuppc_datadir}/pc",
           "${backuppc_datadir}/cpool",
           "${backuppc_datadir}/pool",
        ]:
            ensure  => directory,
            mode    => "0750",
            owner   => "backuppc",
            group   => "root",
            seltype => "var_lib_t",
            require => Package["BackupPC"],
        }
        selinux::manage_fcontext { "${backuppc_datadir}(/.*)?":
            type   => "var_lib_t",
            before => File[$backuppc_datadir],
        }
        file { "/var/lib/BackupPC":
            ensure  => $backuppc_datadir,
            force   => true,
            backup  => ".orig",
            require => File[$backuppc_datadir],
            before  => File["/var/lib/BackupPC/.ssh"],
        }
    }

    apache::configfile { "BackupPC.conf":
        http    => false,
        source  => "puppet:///modules/backuppc/backuppc-httpd.conf",
        require => Package["BackupPC"],
    }

    file { "/usr/share/BackupPC/sbin/.htaccess":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        source  => [ "puppet:///files/backuppc/htaccess",
                     "puppet:///modules/backuppc/htaccess", ],
        require => Package["BackupPC"],
    }

    file { "/usr/share/BackupPC/sbin/BackupPC_Admin":
        ensure  => present,
        mode    => "4750",
        owner   => "backuppc",
        group   => $apache::sslserver::group,
        require => Package["BackupPC"],
    }

    case $::operatingsystem {
        "centos","redhat": {
            case $::operatingsystemrelease {
                /^[1-5]/: {
                    $backuppc_seltype = "httpd_sys_script_rw_t"
                }
                default: {
                    $backuppc_seltype = "httpd_sys_rw_content_t"
                    selinux::manage_fcontext { "/etc/BackupPC(/.*)?":
                        type => $backuppc_seltype,
                    }
                }
            }
        }
        default: {
            $backuppc_seltype = "httpd_sys_rw_content_t"
        }
    }

    file { "/etc/BackupPC/apache.users":
        ensure  => present,
        mode    => "0640",
        owner   => "root",
        group   => $apache::sslserver::group,
        seltype => $backuppc_seltype,
        require => Package["BackupPC"],
    }

    file { "/etc/BackupPC/config.pl":
        ensure  => present,
        source  => "puppet:///files/backuppc/config.pl",
        mode    => "0440",
        owner   => "backuppc",
        group   => "backuppc",
        seltype => $backuppc_seltype,
        require => Package["BackupPC"],
        notify  => Service["backuppc"],
    }

    file { "/etc/BackupPC/hosts.in":
        ensure  => present,
        source  => [ "puppet:///files/backuppc/hosts.in",
                     "puppet:///modules/backuppc/hosts.in", ],
        mode    => "0644",
        owner   => "root",
        group   => "backuppc",
        seltype => $backuppc_seltype,
        require => Package["BackupPC"],
        notify  => Exec["generate-backuppc-hosts"],
    }

    file { "/etc/BackupPC/pc":
        ensure  => directory,
        purge   => true,
        force   => true,
        recurse => true,
        mode    => "0640",
        owner   => "root",
        group   => "backuppc",
        seltype => $backuppc_seltype,
        source  => "puppet:///modules/custom/empty",
        require => Package["BackupPC"],
        notify  => Exec["generate-backuppc-hosts"],
    }

    exec { "generate-backuppc-hosts":
        command     => "/bin/sh -c '(cat /etc/BackupPC/hosts.in ; find /etc/BackupPC/pc -name \\*.pl -exec basename {} .pl \\; | sed -e \"s/\$/ 0 adm/\") > /etc/BackupPC/hosts'",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        refreshonly => true,
        require     => File["/etc/BackupPC/hosts.in"],
        notify      => Service["backuppc"],
    }

    File <<| tag == "backuppc" |>> {
        seltype => $backuppc_seltype,
    }

    service { "backuppc":
        ensure  => running,
        enable  => true,
        require => Package["BackupPC"],
    }

    file { "/var/lib/BackupPC/.ssh":
        ensure  => directory,
        mode    => "0750",
        owner   => "root",
        group   => "backuppc",
        require => Package["BackupPC"],
    }
    file { "/var/lib/BackupPC/.ssh/id_rsa":
        ensure  => present,
        source  => "/etc/ssh/ssh_host_rsa_key",
        mode    => "0640",
        owner   => "root",
        group   => "backuppc",
        require => File["/var/lib/BackupPC/.ssh"],
        before  => Service["backuppc"],
    }
    file { "/var/lib/BackupPC/.ssh/id_rsa.pub":
        ensure  => present,
        source  => "/etc/ssh/ssh_host_rsa_key.pub",
        mode    => "0640",
        owner   => "root",
        group   => "backuppc",
        require => File["/var/lib/BackupPC/.ssh"],
        before  => Service["backuppc"],
    }
    @@ssh_authorized_key { "backuppc":
        ensure  => present,
        key     => $sshrsakey,
        type    => "ssh-rsa",
        user    => "root",
        target  => "/root/.ssh/authorized_keys",
        tag     => "backuppc",
    }

}
