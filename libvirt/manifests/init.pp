# Install libvirt software.
#
class libvirt::client {

    include user::system
    realize(User["qemu"], Group["qemu"])

    file { "/var/lib/qemu":
        ensure => directory,
        mode   => "0700",
        owner  => "qemu",
        group  => "qemu",
    }

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { [ "libvirt", "virt-manager", "virt-viewer", ]:
                ensure  => installed,
                require => File["/var/lib/qemu"],
            }
        }
        default: {
            fail("Not supported on ${::operatingsystem}.")
        }
    }

}


# Configure libvirt guest.
#
class libvirt::guest {

    Libvirt::Facts <<| title == $::homename |>>
    Libvirt::Facts <<| title == $::hostname |>>

}


# Install and configure KVM and libvirtd.
#
# === Global variables
#
#   $libvirt_admingroup:
#       Group which has access to system libvirtd.
#
#   $libvirt_guest_on_boot:
#       Action to taken on host boot [start, ignore] (default: start)
#
#   $libvirt_guest_on_shutdown:
#       Action to taken on host shutdown [suspend, shutdown] (default: suspend)
#
#   $libvirt_parallel_shutdown:
#       If set to non-zero, shutdown will suspend guests concurrently. (default: 0)
#
class libvirt::kvm inherits libvirt::client {

    if !$libvirt_admingroup {
        $libvirt_admingroup = "root"
    }

    if !$libvirt_guest_on_boot {
        $libvirt_guest_on_boot = "start"
    }

    if !$libvirt_guest_on_shutdown {
        $libvirt_guest_on_shutdown = "suspend"
    }

    if !$libvirt_parallel_shutdown {
        $libvirt_parallel_shutdown = 0
    }

    case $::operatingsystem {
        "centos","redhat": {
            case $::operatingsystemrelease {
                /^5\./: {
                    $package = [ "kvm", "kmod-kvm" ]
                }
                /^6\./: {
                    $package = [ "qemu-kvm", "ruby-libvirt" ]
                }
                default: {
                    $package = [ "qemu-kvm", "rubygem-ruby-libvirt", "virt-install" ]
                }
            }
            package { $package:
                ensure  => installed,
                before  => Service["libvirtd"],
                require => User["qemu"],
            }
            file { "/etc/sysconfig/libvirt-guests":
                ensure  => present,
                mode    => "0644",
                owner   => "root",
                group   => "root",
                content => template("libvirt/sysconfig-libvirt-guests.erb"),
                require => Package["libvirt"],
            }
        }
        "fedora": {
            package { "qemu-kvm":
                ensure  => installed,
                before  => Service["libvirtd"],
                require => User["qemu"],
            }
            package { "ruby-libvirt":
                ensure => installed,
            }
        }
        default: {
            fail("Operating system not supported")
        }
    }

    augeas { "libvirtd.conf":
        incl    => "/etc/libvirt/libvirtd.conf",
        lens    => "Simplevars.lns",
        changes => [
            "set unix_sock_group '\"${libvirt_admingroup}\"'",
            "set unix_sock_ro_perms '\"0770\"'",
            "set unix_sock_rw_perms '\"0770\"'",
            "set auth_unix_ro '\"none\"'",
            "set auth_unix_rw '\"none\"'",
        ],
        require => Package["libvirt"],
        notify  => Service["libvirtd"],
    }

    if $::interfaces =~ /virbr[0-9]/ and defined(Service["iptables"]) {
        $iptables = Service["iptables"]
    } else {
        $iptables = undef
    }

    service { "libvirtd":
        ensure    => running,
        enable    => true,
        subscribe => $iptables,
    }

    if $::libvirt_activedomains {
        $domains = split($::libvirt_activedomains, ',')
        @@libvirt::facts { $domains:
            host => $::homename,
        }
    }

}


# Set libvirt external facts.
#
# === Parameters
#
#   $host:
#       libvirt host name.
#
define libvirt::facts($host) {

    file { "/etc/facter/facts.d/libvirt.txt":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        content => "libvirt_host=${host}\n",
    }

}
