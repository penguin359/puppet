
# Enable process accounting.
#
class psacct {

    case $::kernel {
        "linux": {
            include psacct::linux
        }
        "openbsd": {
            include psacct::openbsd
        }
        default: {
            fail("psacct module not supported in ${::kernel}")
        }
    }

}


# Enable process accounting on Linux hosts.
#
class psacct::linux {

    package { "psacct":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "acct",
            "ubuntu" => "acct",
            default  => "psacct",
        },
    }

    service { "psacct":
        ensure    => $::operatingsystem ? {
            "debian" => undef,
            "ubuntu" => undef,
            default  => running,
        },
        enable    => true,
        hasstatus => true,
        name      => $::operatingsystem ? {
            "debian" => "acct",
            "ubuntu" => "acct",
            default  => "psacct",
        },
        require   => Package["psacct"],
    }

}


# Enable process accounting on OpenBSD hosts.
#
class psacct::openbsd {

    file { "/var/account":
        ensure => directory,
        mode   => "0750",
        owner  => "root",
        group  => "wheel",
    }

    file { "/var/account/acct":
        ensure => present,
        mode   => "0640",
        owner  => "root",
        group  => "wheel",
        notify => Exec["accton"],
    }

    service { "accounting":
        enable  => true,
        require => File["/var/account/acct"],
    }

    exec { "accton":
        command     => "accton /var/account/acct",
        path        => "/bin:/usr/bin:/sbin:/usr/sbin",
        user        => "root",
        refreshonly => true,
    }

}
