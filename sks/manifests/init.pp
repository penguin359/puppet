# Install and configure SKS keyserver.
#
class sks($datadir=undef) {

    case $::operatingsystem {
        "ubuntu": {
            $user = "debian-sks"
            $group = "debian-sks"
            $config = "/etc/sks/sksconf"
        }
        default: {
            fail("sks not supported on ${::operatingsystem}")
        }
    }

    package { "sks":
        ensure => installed,
    }

    if $datadir {
        file { $datadir:
            ensure  => directory,
            mode    => "0700",
            owner   => $user,
            group   => $group,
            require => Package["sks"],
        }
        file { "/var/lib/sks":
            ensure  => link,
            target  => $datadir,
            force   => true,
            require => File[$datadir],
        }
    } else {
        file { "/var/lib/sks":
            ensure  => directory,
            mode    => "0700",
            owner   => $user,
            group   => $group,
            require => Package["sks"],
        }
    }

    exec { "sks build":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        user    => $user,
        creates => "/var/lib/sks/DB",
        require => File["/var/lib/sks"],
        before  => Service["sks"],
    }

    file { $config:
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        content => template("sks/sksconf.erb"),
        require => Package["sks"],
        notify  => Service["sks"],
    }

    case $::operatingsystem {
        "debian","ubuntu": {
            augeas { "enable-sks":
                context => "/files/etc/default/sks",
                changes => "set initstart yes",
                require => Package["sks"],
                notify  => Service["sks"],
            }
        }
        default: { }
    }

    file { "/var/lib/sks/www":
        ensure => directory,
        mode   => "0700",
        owner  => $user,
        group  => $group,
    }

    file { "/var/lib/sks/www/index.html":
         ensure  => present,
         mode    => "0600",
         owner   => $user,
         group   => $group,
         content => template("sks/index.html.erb"),
         before  => Service["sks"],
    }

    service { "sks":
        ensure => running,
        enable => true,
        status => "pgrep -f /usr/sbin/sks",
    }

}
