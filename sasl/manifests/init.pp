
# Install sasl client
#
class sasl::client {

    package { "cyrus-sasl":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "sasl2-bin",
            "ubuntu" => "sasl2-bin",
            default  => "cyrus-sasl",
        },
        flavor => $::operatingsystem ? {
            "openbsd" => "ldap",
            default   => undef,
        },
    }

    if $::kerberos_realm and $::operatingsystem != "OpenBSD" {
        package { "cyrus-sasl-gssapi":
            ensure => installed,
            name   => $::operatingsystem ? {
                "debian" => "libsasl2-modules-gssapi-mit",
                "ubuntu" => "libsasl2-modules-gssapi-mit",
                default  => "cyrus-sasl-gssapi",
            },
        }
    }

}

# Install saslauthd daemon.
#
# === Global variables
#
#   $saslauthd_mech:
#       Authentication mechanism to use. Defaults to system
#       default. Supported mechanisms include pam, ldap and kerberos5.
#
#       For ldap authentication, see ldap::client for required global variables.
#
#       When using kerberos5 mech on CentOS, Fedora or RedHat system
#       the saslauthd is ran as saslauth user and uses host/$FQDN from
#       /etc/saslauthd.keytab for authentication.
#
class sasl::saslauthd {

    require sasl::client

    case $saslauthd_mech {
        "","pam": { }
        "ldap": {
            include ldap::client

            case $::operatingsystem {
                "centos","fedora","redhat": {
                    augeas { "set-saslauthd-mech":
                        context => "/files/etc/sysconfig/saslauthd",
                        changes => "set MECH ldap",
                        notify  => Service["saslauthd"],
                    }
                    $user = "saslauth"
                }
                "openbsd": {
                    Service["saslauthd"] {
                        start => "/usr/local/sbin/saslauthd -a ldap",
                    }
                }
                "ubuntu": {
                    augeas { "set-saslauthd-mech":
                        context => "/files/etc/default/saslauthd",
                        changes => "set MECHANISMS ldap",
                        notify  => Service["saslauthd"],
                    }
                }
                default: { }
            }

            file { "/etc/saslauthd.conf":
                ensure => present,
                mode    => "0644",
                owner   => "root",
                group   => $::operatingsystem ? {
                    "openbsd" => "wheel",
                    default   => "root",
                },
                content => template("sasl/saslauthd.conf.ldap.erb"),
                notify  => Service["saslauthd"],
            }
        }
        "kerberos5": {
            case $::operatingsystem {
                "centos","fedora","redhat": {
                    augeas { "set-saslauthd-mech":
                        context => "/files/etc/sysconfig/saslauthd",
                        changes => [ "set MECH kerberos5",
                                     "set KRB5_KTNAME '\"/etc/saslauthd.keytab\"'", ],
                        notify  => Service["saslauthd"],
                    }
                    if versioncmp($::operatingsystemrelease, 7) < 0 {
                        augeas { "export-saslauthd-keytab":
                            context => "/files/etc/sysconfig/saslauthd",
                            changes => "set @export KRB5_KTNAME",
                            notify  => Service["saslauthd"],
                        }
                    }
                    $user = "saslauth"
                }
                "openbsd": {
                    Service["saslauthd"] {
                        start => "/usr/local/sbin/saslauthd -a kerberos5",
                    }
                }
                "ubuntu": {
                    augeas { "set-saslauthd-mech":
                        context => "/files/etc/default/saslauthd",
                        changes => "set MECHANISMS kerberos5",
                        notify  => Service["saslauthd"],
                    }
                }
                default: { }
            }
        }
        default: {
            fail("Unknown mechanism ${saslauthd_mech} for sasl::saslauthd")
        }
    }

    if $::operatingsystem == "Ubuntu" {
        augeas { "enable-saslauthd":
            context => "/files/etc/default/saslauthd",
            changes => "set START yes",
            before  => Service["saslauthd"],
        }
    }

    if $user {
        case $::operatingsystem {
            "centos","fedora","redhat": {
                file { "/var/run/saslauthd":
                    ensure => directory,
                    mode   => "0775",
                    owner  => "root",
                    group  => $user,
                    before => Service["saslauthd"],
                }
                if versioncmp($::operatingsystemrelease, "7") < 0 {
                    augeas { "set-saslauthd-user":
                        context => "/files/etc/sysconfig/saslauthd",
                        changes => "set DAEMONOPTS '\"--user ${user}\"'",
                        notify  => Service["saslauthd"],
                    }
                } else {
                    file { "/etc/systemd/system/saslauthd.service.d":
                        ensure  => directory,
                        mode    => "0644",
                        owner   => "root",
                        group   => "root",
                        require => Class["sasl::client"],
                    }
                    file { "/etc/systemd/system/saslauthd.service.d/user.conf":
                        ensure  => present,
                        content => "[Service]\nUser=${user}\n",
                        mode    => "0644",
                        owner   => "root",
                        group   => "root",
                        require => File["/etc/systemd/system/saslauthd.service.d"],
                        notify  => Service["saslauthd"],
                    }
                    file { "/usr/lib/tmpfiles.d/saslauthd.conf":
                        ensure  => present,
                        content => "d /run/saslauthd 0775 root ${user} -\n",
                        mode    => "0644",
                        owner   => "root",
                        group   => "root",
                        require => Class["sasl::client"],
                    }
                }
            }
            default: {
                fail("Running saslauthd as non root not supported on ${::operatingsystem}")
            }
        }
    } else {
        $user = "root"
    }

    service { "saslauthd":
        ensure  => running,
        enable  => true,
    }

    file { "/etc/sasldb2":
        ensure  => present,
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Exec["generate-sasldb2"],
        before  => Service["saslauthd"],
    }
    exec { "generate-sasldb2":
        command => "saslpasswd2 -d foobar ; true",
        path    => "/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin:/usr/local/sbin",
        creates => "/etc/sasldb2",
    }

}


# Install saslauthd service config
#
# === Parameters
#
#   $name:
#       Service name.
#
#   $content:
#       Content for service config file. Default is to use file from
#       fileserver.
#
# === Sample usage
#
# sasl::saslauthd::service { "Sendmail": }
#
define sasl::saslauthd::service($content=undef) {

    include sasl::saslauthd

    case $::operatingsystem {
        "centos","fedora","redhat": {
            case $::architecture {
                "i386":   { $libdir = "/usr/lib/sasl2" }
                "x86_64": { $libdir = "/usr/lib64/sasl2" }
                default:  { fail("Unknown architecture ${::architecture}") }
            }
        }
        "openbsd": {
            $libdir = "/usr/local/lib/sasl2"
        }
        "ubuntu": {
            $libdir = "/usr/lib/sasl2"
        }
        default: {
            fail("sasl not supported on ${::operatingsystem}")
        }
    }

    file { "${libdir}/${name}.conf":
        ensure  => present,
        content => $content,
        source  => $content ? {
            undef   => [ "puppet:///files/sasl/${name}.${::homename}.conf",
                         "puppet:///files/sasl/${name}.conf",
                         "puppet:///files/sasl/service.conf",
                         "puppet:///modules/sasl/service.conf", ],
            default => undef,
        },
        mode    => "0644",
        owner   => "root",
        group   => $::operatingsystem ? {
            "openbsd" => "wheel",
            default   => "root",
        },
        require => Package["cyrus-sasl"],
        notify  => Service["saslauthd"],
    }

}

