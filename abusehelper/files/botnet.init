#!/bin/sh

# chkconfig: 2345 95 05
# description: AbuseHelper botnets
# processname: botnet

### BEGIN INIT INFO
# Provides: botnet
# Required-Start: $local_fs $network $syslog
# Should-Start:
# Required-Stop:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: AbuseHelper botnets
# Description: AbuseHelper botnets
### END INIT INFO

if [ $(id -u) != "0" ]; then
    echo "This script must be run with root privileges." && exit 1
fi

if [ -s /etc/default/botnet ]; then
    . /etc/default/botnet
elif [ -s /etc/sysconfig/botnet ]; then
    . /etc/sysconfig/botnet
fi

if [ -z "${BOTUSER}" ]; then
    echo "$0: no BOTUSER defined"
    exit 1
fi

if [ -z "${BOTNETS}" ]; then
    echo "$0: no BOTNETS defined"
    exit 1
fi

botnet_action() {
    action="$1"
    botnet="$2"

    echo -n "${botnet}: "

    if [ -d ${botnet} ]; then
        startup="."
    elif [ -f ${botnet} ]; then
        startup="$(basename ${botnet})"
        botnet="$(dirname ${botnet})"
    else
        echo "No such file or directory."
        return 1
    fi

    su -s /bin/sh - ${BOTUSER} \
        -c "umask 007 ; cd ${botnet} && botnet ${action} ${startup}"
}

start_botnets() {
    set -- ${BOTNETS}
    while [ $# -gt 0 ]; do
        botnet_action start ${1}
        shift
        test -n "${1}" && sleep 10
    done
}

stop_botnets() {
    for botnet in ${BOTNETS}; do
        botnet_action stop ${botnet}
    done
}

restart_botnets() {
    set -- ${BOTNETS}
    while [ $# -gt 0 ]; do
        botnet_action restart ${1}
        shift
        test -n "${1}" && sleep 10
    done
}

case "$1" in
    start)
        start_botnets
        ;;
    stop)
        stop_botnets
        ;;
    restart)
        restart_botnets
        ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
        ;;
esac

exit 0
