# Install ejabberd.
#
# === Parameters
#
# $collab:
#   Boolean for enabling collab integration. Defaults to false.
#
# $package:
#   Ejabberd package source. Required for collab integration.
#
# $hosts:
#   Array of domains serverd by ejabberd. Defaults to [ "$homename" ].
#
# $admins:
#   Array of users with admin privileges.
#
# $webhosts:
#   Array of BOSH virtual hosts.
#
# $auth:
#   Authentication method or array of multiple methods.
#   Valid values internal, external or ldap. Defaults to internal.
#
# $extauth:
#   Path to external authentication command.
#
# $muclog_datadir:
#   Path where to store chatroom logs. Disabled by default.
#
# $muclog_format:
#   Chatroom log format. Valid values html or plaintext.
#
# $ssl_key:
#   Path to SSL private key.
#
# $ssl_cert:
#   Path to SSL certificate.
#
# $ssl_chain:
#   Path to SSL certificate chain.
#
# $ldap_server:
#   Array of LDAP authentication servers.
#
# $ldap_basedn:
#   LDAP base dn.
#
# $ldap_encrypt:
#   LDAP encryption. Defaults to "tls".
#
# $ldap_port:
#   LDAP port. Defaults to 636.
#
# $ldap_uid:
#   LDAP UID attribute. Defaults to "uid".
#
# $ldap_rootdn:
#   Optional bind DN.
#
# $ldap_password:
#   Bind DN password.
#
class ejabberd(
  $collab=false,
  $package=undef,
  $hosts=[$::homename],
  $admins=undef,
  $webhosts=undef,
  $auth='internal',
  $extauth=undef,
  $muclog_datadir=undef,
  $muclog_format='plaintext',
  $ssl_key="${::puppet_ssldir}/private_keys/${::homename}.pem",
  $ssl_cert="${::puppet_ssldir}/certs/${::homename}.pem",
  $ssl_chain=undef,
  $ldap_server=undef,
  $ldap_basedn=undef,
  $ldap_encrypt='tls',
  $ldap_port='636',
  $ldap_uid='uid',
  $ldap_rootdn=undef,
  $ldap_password=undef
) {

  include user::system
  realize(User['ejabberd'], Group['ejabberd'])

  if ! ($muclog_format in [ 'html', 'plaintext' ]) {
    fail("Invalid value ${muclog_format} for muclog_format")
  }

  case $::operatingsystem {
    'centos','redhat','fedora': {
      $package_provider = 'rpm'
    }
    'debian','ubuntu': {
      $package_provider = 'dpkg'
    }
    default: {
      fail("ejabberd not supported on ${::operatingsystem}.")
    }
  }

  if $package and versioncmp($package, 'ejabberd-13.10') >= 0 {
    if $::operatingsystem != 'CentOS' {
      fail("ejabberd ${package} not supported on ${::operatingsystem}")
    }
    $config = 'ejabberd.yml'
    $erlang_solutions = true
    package { 'libyaml':
      ensure => installed,
      before => Package['ejabberd'],
    }
  } else {
    $config = 'ejabberd.cfg'
    $erlang_solutions = false
  }

  class { 'erlang':
    erlang_solutions => $erlang_solutions,
    before           => Package['ejabberd'],
  }

  if $collab == true {
    if ! $package {
      fail('Must define package for collab integration')
    }

    file { "/usr/local/src/${package}":
      ensure => present,
      mode   => '0644',
      owner  => 'root',
      group  => 'root',
      source => "puppet:///files/packages/${package}",
      before => Package['ejabberd'],
    }

    Package['ejabberd'] {
      provider => $package_provider,
      source   => "/usr/local/src/${package}",
    }

    exec { 'usermod-ejabberd':
      path    => '/bin:/usr/bin:/sbin:/usr/sbin',
      command => 'usermod -a -G collab ejabberd',
      unless  => "id -n -G ejabberd | grep '\\bcollab\\b'",
      require => [ User['ejabberd'], Group['collab'] ],
      notify  => Service['ejabberd'],
    }

    Service['ejabberd'] {
      require => Class['wiki::collab'],
    }

    if $muclog_datadir {
      file { $muclog_datadir:
        ensure  => directory,
        mode    => '2770',
        owner   => 'collab',
        group   => 'collab',
        require => User['collab'],
        before  => Service['ejabberd'],
      }
    }
  }

  package { 'ejabberd':
    ensure  => $collab ? {
      true    => latest,
      default => installed,
    },
    require => [ User['ejabberd'], Group['ejabberd'] ],
  }

  service { 'ejabberd':
    ensure => running,
    enable => true,
    status => 'ejabberdctl status >/dev/null',
  }

  include ssl

  file { "${ssl::private}/ejabberd.key":
    ensure => present,
    source => $ssl_key,
    mode   => '0600',
    owner  => 'root',
    group  => 'root',
    notify => Exec['generate-ejabberd-pem'],
  }
  file { "${ssl::certs}/ejabberd.crt":
    ensure => present,
    source => $ssl_cert,
    mode   => '0644',
    owner  => 'root',
    group  => 'root',
    notify => Exec['generate-ejabberd-pem'],
  }
  if $ssl_chain {
    file { "${ssl::certs}/ejabberd.chain.crt":
      ensure => present,
      source => $ssl_chain,
      mode   => '0644',
      owner  => 'root',
      group  => 'root',
      notify => Exec['generate-ejabberd-pem'],
    }
    $cert_files = "${ssl::private}/ejabberd.key ${ssl::certs}/ejabberd.crt ${ssl::certs}/ejabberd.chain.crt"
  } else {
    $cert_files = "${ssl::private}/ejabberd.key ${ssl::certs}/ejabberd.crt"
  }

  exec { 'generate-ejabberd-pem':
    path        => '/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin',
    command     => "/bin/sh -c 'umask 077 ; cat ${cert_files} > /etc/ejabberd/ejabberd.pem'",
    refreshonly => true,
    before      => File['/etc/ejabberd/ejabberd.pem'],
    require     => Package['ejabberd'],
    notify      => Service['ejabberd'],
  }

  file { '/etc/ejabberd/ejabberd.pem':
    ensure  => present,
    mode    => '0640',
    owner   => 'root',
    group   => 'ejabberd',
    require => Package['ejabberd'],
  }

  file { "/etc/ejabberd/${config}":
    ensure  => present,
    mode    => '0640',
    owner   => 'root',
    group   => 'ejabberd',
    content => template("ejabberd/${config}.erb"),
    require => Package['ejabberd'],
    notify  => Service['ejabberd'],
  }

  case $::operatingsystem {
    'debian', 'ubuntu': {
      augeas { 'set-ejabberd-default':
        context => '/files/etc/default/ejabberd',
        changes => [ 'set POLL true', 'set SMP auto' ],
        require => Package['ejabberd'],
        notify  => Service['ejabberd'],
      }
    }
    default: { }
  }

  $htdocs = '/usr/share/ejabberd/htdocs'

  if $webhosts {
    include apache::mod::proxy
    include apache::mod::proxy_http
    include apache::mod::rewrite

    file { $htdocs:
      ensure  => directory,
      mode    => '0755',
      owner   => 'root',
      group   => 'root',
      require => Package['ejabberd'],
    }

    file { "${htdocs}/.htaccess":
      ensure  => present,
      mode    => '0644',
      owner   => 'root',
      group   => 'root',
      content => template('ejabberd/htaccess.erb'),
    }

    apache::configfile { 'ejabberd.conf':
      http   => false,
      source => 'puppet:///modules/ejabberd/ejabberd-httpd.conf',
    }

    selinux::manage_port { '5280':
      type  => 'http_port_t',
      proto => 'tcp',
    }

    ejabberd::configwebhost { $webhosts:
      htdocs => $htdocs,
    }
  }

}


# Enable bosh on virtual host.
#
define ejabberd::configwebhost($htdocs) {

  file { "/srv/www/https/${name}/bosh":
    ensure => link,
    target => $htdocs,
  }

}


# Install ejabberd backup cron script.
#
# === Parameters
#
# $datadir:
#   Path where to store the backups. Defaults to "/srv/ejabberd-backup".
#
class ejabberd::backup(
  $datadir='/srv/ejabberd-backup',
) {

  file { $datadir:
    ensure => directory,
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  file { '/usr/local/sbin/ejabberd-backup':
    ensure  => present,
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
    content => template('ejabberd/ejabberd-backup.erb'),
  }

  cron { 'ejabberd-backup':
    ensure  => present,
    command => '/usr/local/sbin/ejabberd-backup',
    user    => 'root',
    minute  => '15',
    hour    => '21',
    require => File[$datadir, '/usr/local/sbin/ejabberd-backup'],
  }

}
