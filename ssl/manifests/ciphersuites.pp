#
# Changelog:
#
#   Lets keep changelog for this file for easier auditing. Newest
#   changes on top.
#
#   2014-04-14, oherrala
#     Mozilla's recommended cipersuite list (document version 2.5.1)
#   2014-07-21, osalmi
#     Update to Version 3.1
#   2014-12-19, osalmi
#     Use the Intermediate compatibility configuration (Version 3.4)
#
class ssl::ciphersuites {

  #
  # Mozilla (document version 3.4)
  #
  #   Ref: https://wiki.mozilla.org/Security/Server_Side_TLS#Backward_Compatible_Ciphersuite
  #
  $mozilla_ciphersuites = 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:AES:CAMELLIA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!MD5:!PSK:!aECDH:!EDH-DSS-DES-CBC3-SHA:!EDH-RSA-DES-CBC3-SHA:!KRB5-DES-CBC3-SHA'

  #
  # Default ciphersuites
  #
  #   To overwrite default ciphersuites, add $site_ciphersuites with
  #   list of you preferred ones.
  #
  $default_ciphersuites = $mozilla_ciphersuites

}
