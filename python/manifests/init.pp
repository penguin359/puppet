# Install python
#
class python {

    package { "python":
        ensure => installed,
        notify => $::operatingsystem ? {
            "openbsd" => Exec["python-links"],
            default   => undef,
        },
    }

    exec { "python-links":
        path        => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        command     => "pkg_info python | egrep '^[ ]*ln -sf' | sh",
        user        => "root",
        group       => "wheel",
        refreshonly => true,
    }

}


# Install python 2.6 for RHEL/CentOS 5
#
class python::python26 {

    if $::operatingsystem in ["CentOS","RedHat"] and versioncmp($::operatingsystemrelease, "6") < 0 {
        package { "python26":
            ensure => installed,
        }
    }

}


# Install m2crypto module for python.
#
class python::m2crypto {

    package { "m2crypto":
        ensure => installed,
        name   => $::operatingsystem ? {
            "debian" => "python-m2crypto",
            "ubuntu" => "python-m2crypto",
            default  => "m2crypto",
        }
    }

}


# Install python software using setup.py.
#
# === Parameters
#
#   $name:
#       Source directory.
#   $python:
#       Python executable name. Defaults to python.
#   $source:
#       Source path to package archive.
#
# === Sample usage
#
# python::setup::install { "/usr/local/src/moin-1.8.8":
#   source => "puppet:///files/packages/moin-1.8.8.tar.gz",
# }
#
define python::setup::install(
    $python="python",
    $source=undef,
) {

    if $source {
        $filename = basename($source)
        file { "/usr/local/src/${filename}":
            ensure => present,
            mode   => "0644",
            owner  => "root",
            group  => $::operatingsystem ? {
                "openbsd" => "wheel",
                default   => "root",
            },
            source => $source,
        }
        util::extract::tar { $name:
            ensure  => latest,
            strip   => "1",
            source  => "/usr/local/src/${filename}",
            require => File["/usr/local/src/${filename}"],
            before  => Exec["python-setup-install-${name}"],
        }
    }

    exec { "python-setup-install-${name}":
        path    => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        cwd     => $name,
        command => "/bin/sh -c 'umask 022; ${python} setup.py install && mkdir -p build'",
        creates => "${name}/build",
    }

}


# Compile python script into binary form.
#
# === Parameters
#
#   $name:
#       File name to compile.
#
# === Sample usage
#
# python::compile { "/usr/lib/python2.4/site-packages/dynldap.py": }
#
define python::compile() {

    exec { "python -c \"import py_compile; py_compile.compile('${name}')\"":
        path   => "/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin",
        onlyif => "test '${name}' -nt '${name}c'",
    }

}
