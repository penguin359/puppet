# Install erlang.
#
# === Parameters
#
# $erlang_solutions:
#   Use Erlang Solutions repositories. Defaults to false.
#
class erlang(
  $erlang_solutions=false,
) {

  if $erlang_solutions == true {
    case $::operatingsystem {
      'centos': {
        require yum::repo::erlang_solutions
      }
      default: {
        fail("Erlang Solutions not supported on ${::operatingsystem}.")
      }
    }
  }

  case $::operatingsystem {
    'centos','redhat','fedora': {
      package { 'erlang':
        ensure => installed,
      }
    }
    'debian','ubuntu': {
      package { [ 'erlang', 'erlang-base' ]:
        ensure => installed,
      }
    }
    default: {
      fail("erlang not supported on ${::operatingsystem}.")
    }
  }

}
