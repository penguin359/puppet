
# Set system locale
#
# === Parameters
#
#     $lang:
#         Value to set into $LANG environment. Defaults to en_US.
#
# === Sample usage
#
# class { "locale":
#     lang => "en_US.ISO8859-1",
# }
#
class locale($lang="en_US") {

    case $::operatingsystem {
        "centos": {
            augeas { "i18n":
                context => "/files/etc/sysconfig/i18n",
                changes => "set LANG ${lang}",
            }
        }
        "fedora": {
            augeas { "i18n":
                context => $::operatingsystemrelease ? {
                    /^1[0-7]/ => "/files/etc/sysconfig/i18n",
                    default   => "/files/etc/locale.conf",
                },
                changes => "set LANG ${lang}",
            }
        }
        "debian","ubuntu": {
            augeas { "i18n":
                context => "/files/etc/default/locale",
                changes => "set LANG ${lang}",
            }
        }
        default: {
            fail("locale module not supported in ${::operatingsystem}")
        }
    }

}

