# Install OpenJDK development environment.
#
class openjdk::jdk {

  case $::operatingsystem {
    'centos','redhat': {
      package { 'java-1.7.0-openjdk-devel':
        ensure => installed,
      }
    }
    'ubuntu': {
      package { 'openjdk-7-jdk':
        ensure => installed,
      }
    }
    default: {
      fail("openjdk not supported on ${::operatingsystem}")
    }
  }

}


# Install OpenJDK runtime environment.
#
class openjdk::jre {

  case $::operatingsystem {
    'centos','redhat': {
      package { 'java-1.7.0-openjdk':
        ensure => installed,
      }
    }
    'ubuntu': {
      package { 'openjdk-7-jre':
        ensure => installed,
      }
    }
    default: {
      fail("openjdk not supported on ${::operatingsystem}")
    }
  }

}
