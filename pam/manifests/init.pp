
# Common pam prequisites
#
class pam::common {

    case $::operatingsystem {
        "centos","redhat","fedora": {
            package { "authconfig":
                ensure => installed,
            }
        }
        "ubuntu": {
            package { [ "libpam-runtime", "libpam-modules", ]:
                ensure => installed,
            }
            exec { "pam-auth-update":
                path        => "/bin:/usr/bin:/sbin:/usr/sbin",
                refreshonly => true,
                require     => Package["libpam-runtime"],
            }
        }
        default: { }
    }

}


# Enable pam_mkhomedir module
#
#   $umask:
#     The user file-creation mask is set to mask. The default value of
#     mask is 0077.
#
class pam::mkhomedir($umask="0077") {

  include pam::common

  case $::operatingsystem {
    "centos","redhat","fedora": {
      exec { "authconfig --enablemkhomedir --update":
        path    => "/bin:/usr/bin:/sbin:/usr/sbin",
        unless  => "egrep '^USEMKHOMEDIR=yes\$' /etc/sysconfig/authconfig",
        require => Package["authconfig"],
      }
      augeas { "system-auth-pam_mkhomedir-umask":
        context => "/files/etc/pam.d/system-auth/",
        changes => "set *[module='pam_mkhomedir.so']/argument umask=${umask}",
        require => Exec["authconfig --enablemkhomedir --update"],
      }
      augeas { "password-auth-pam_mkhomedir-umask":
        context => "/files/etc/pam.d/password-auth/",
        changes => "set *[module='pam_mkhomedir.so']/argument umask=${umask}",
        require => Exec["authconfig --enablemkhomedir --update"],
      }
    }
    "ubuntu": {
      file { "/usr/share/pam-configs/pam_mkhomedir":
        content => template("pam/pam_mkhomedir.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => [ Package["libpam-runtime"], Package["libpam-modules"], ],
        notify  => Exec["pam-auth-update"],
      }
    }
    default: { }
  }

}


# Enable pam_mount module
#
# === Parameters:
#
#     $source:
#         Path to pam_mount.conf.xml file to use.
#
class pam::mount($source) {

    include pam::common

    package { "pam_mount":
        ensure => installed,
        name   => $::operatingsystem ? {
            "ubuntu" => "libpam-mount",
            default  => "pam_mount",
        },
        notify => Exec["pam-auth-update"],
    }

    file { "/etc/security/pam_mount.conf.xml":
        ensure  => present,
        source  => $source,
        mode    => "0644",
        owner   => "root",
        group   => "root",
        require => Package["pam_mount"],
        before  => Exec["pam-auth-update"],
    }

}
