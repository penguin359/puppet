
# Install yum-updatesd service
#
class yum::updatesd {

    package { "yum-updatesd":
        ensure => installed,
    }

    file { "/etc/yum/yum-updatesd.conf":
        ensure => present,
        source => [ "puppet:///files/yum/yum-updatesd.conf.${::homename}",
                    "puppet:///files/yum/yum-updatesd.conf",
                    "puppet:///modules/yum/yum-updatesd.conf", ],
        mode   => "0644",
        owner  => "root",
        group  => "root",
        notify => Service["yum-updatesd"],
    }

    service { "yum-updatesd":
        ensure    => running,
        enable    => true,
        hasstatus => true,
        require   => Package["yum-updatesd"],
    }

}


# Cron job for automatic downloading of updates
#
class yum::cron::download {

    require yum::plugin::downloadonly

    cron { "yum-cron-download":
        ensure  => present,
        command => "yum -d 0 -e 1 -y --downloadonly update >/dev/null",
        user    => "root",
        hour    => "3",
        minute  => fqdn_rand(60),
    }

}


# Install changelog plugin
#
class yum::plugin::changelog {

    case $::operatingsystem {
        "centos","redhat": {
            $package = $::operatingsystemrelease ? {
                /^[1-5]/ => "yum-changelog",
                default  => "yum-plugin-changelog",
            }
        }
        default: {
            $package = "yum-plugin-changelog"
        }
    }

    package { "yum-plugin-changelog":
        ensure => installed,
        name   => $package,
    }

}


# Install downloadonly plugin
#
class yum::plugin::downloadonly {

    case $::operatingsystem {
        "centos","redhat": {
            $package = $::operatingsystemrelease ? {
                /^[1-5]/ => "yum-downloadonly",
                /^6\.[0-6]$/     => "yum-plugin-downloadonly",
                default  => undef,
            }
        }
        default: {
            $package = "yum-plugin-downloadonly"
        }
    }

    if $package {
        package { "yum-plugin-downloadonly":
            ensure => installed,
            name   => $package,
        }
    }

}


# Install priorities plugin
#
class yum::plugin::priorities {

    case $::operatingsystem {
        "centos","redhat": {
            $package = $::operatingsystemrelease ? {
                /^[1-5]/ => "yum-priorities",
                default  => "yum-plugin-priorities",
            }
        }
        default: {
            $package = "yum-plugin-priorities"
        }
    }

    package { "yum-plugin-priorities":
        ensure => installed,
        name   => $package,
    }

}


# Common prequisites for yum
#
# === Global variables
#
#   $yum_proxy:
#       URL of HTTP proxy, for example "http://www.example.local:80/".
#
class yum::common {

    case $::operatingsystem {
        "fedora": {
            $osname = "fedora"
            $osver = $::operatingsystemrelease
        }
        "centos","redhat": {
            $osname = "el"
            $osver = regsubst($::operatingsystemrelease, '^(\d+)\..*$', '\1')
        }
        default: {
            fail("yum not supported on ${::operatingsystem}")
        }
    }

    if $yum_proxy {
        augeas { "yum-enable-proxy":
            context => "/files/etc/yum.conf/main",
            changes => "set proxy ${yum_proxy}",
            tag     => "bootstrap",
        }
        augeas { "yum-disable-fastestmirror":
            context => "/files/etc/yum/pluginconf.d/fastestmirror.conf/main",
            changes => "set enabled 0",
            tag     => "bootstrap",
        }
    }

}


# Manage yum excludes.
#
# === Global variables
#
#   $yum_exclude:
#       Array of packets to exclude.
#
class yum::exclude {

    if !$yum_exclude {
        fail("\$yum_exclude must be defined for yum::exclude")
    }

    $yum_exclude_real = inline_template('<%= @yum_exclude.join(" ") -%>')

    augeas { "yum-exclude":
        context => "/files/etc/yum.conf/main",
        changes => "set exclude '${yum_exclude_real}'",
    }

}


# Add new yum repository.
#
# === Parameters
#
#   $name:
#       Repository name.
#   $ensure:
#       Present/enable enables repository (default), absent removes
#       repository completely and disable disables repository.
#   $baseurl:
#       Base URL for this repository.
#   $mirrorlist:
#       Mirrorlist URL for this repository. If $baseurl is also set
#       this won't have any effect.
#   $descr:
#       Repository description. Defaults to $name.
#   $gpgkey:
#       Location where GPG signing key can be found.
#   $gpgcheck:
#       Perform GPG signature check for packages. Enabled by default
#       if $gpgkey is set.
#   $repocheck:
#       Perform GPG signature check for repository metadata.
#   $sslcacert:
#       Path to the file containing the certificates of the
#       certificate authorities yum should use to verify TLS
#       connections.
#   $priority:
#       Optional priority for this repository.
#
# === Sample usage
#
# yum::repo { "tmz-puppet":
#     baseurl => "http://tmz.fedorapeople.org/repo/puppet/epel/5/i386/",
#     descr   => "Puppet for EL 5 - i386",
#     gpgkey  => "http://tmz.fedorapeople.org/repo/RPM-GPG-KEY-tmz",
# }
#
define yum::repo(
  $ensure="present",
  $baseurl="",
  $mirrorlist="",
  $descr="",
  $gpgkey="",
  $gpgcheck=true,
  $repocheck=false,
  $sslcacert="",
  $priority=""
) {

    tag("bootstrap")

    # can't use ensure variable inside template :(
    $status = $ensure

    if $status != "absent" {

        if !$baseurl and !$mirrorlist {
            fail("Either \$baseurl or \$mirrorlist needs to be defined for yum::repo")
        }

        if regsubst($gpgkey, "^(puppet://).*", '\1') == "puppet://" {
            file { "/etc/pki/rpm-gpg/RPM-GPG-KEY-${name}":
                ensure => present,
                source => $gpgkey,
                mode   => "0644",
                owner  => "root",
                group  => "root",
                before => File["/etc/yum.repos.d/${name}.repo"],
                notify => Exec["rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-${name}"],
            }
            exec { "rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-${name}":
                path        => "/bin:/usr/bin:/sbin:/usr/sbin",
                user        => "root",
                refreshonly => true,
            }
            $gpgkey_real = "file:///etc/pki/rpm-gpg/RPM-GPG-KEY-${name}"
        } else {
            $gpgkey_real = $gpgkey
        }

        if $descr {
            $descr_real = $descr
        } else {
            $descr_real = $name
        }

        if $priority {
            include yum::plugin::priorities
        }

    }

    file { "/etc/yum.repos.d/${name}.repo":
        ensure  => $status ? {
            "absent" => absent,
            default  => present,
        },
        content => template("yum/yum.repo.erb"),
        mode    => "0644",
        owner   => "root",
        group   => "root",
    }

}


# packagecloud.io repositories
#
# === Parameters
#
#   $name:
#       Repository path under packagecloud.io. For example
#       "basho/riak".
#
define yum::repo::packagecloud() {

  $filename = regsubst($name, '\/', '_')

  yum::repo { $filename:
    descr     => "$name repository from packagecloud.io",
    baseurl   => "https://packagecloud.io/$name/el/\$releasever/\$basearch",
    gpgkey    => "puppet:///modules/yum/keys/packagecloud.io.key",
    gpgcheck  => false,
    repocheck => true,
    sslcacert => "/etc/pki/tls/certs/ca-bundle.crt",
  }

}


# Add Adobe repository
#
class yum::repo::adobe {

    yum::repo { "adobe":
        descr   => "Adobe Systems Incorporated",
        baseurl => "http://linuxdownload.adobe.com/linux/i386/",
        gpgkey  => "puppet:///modules/yum/keys/adobe.key",
    }

    if $architecture == "x86_64" {
        yum::repo { "adobe-x86_64":
            descr   => "Adobe Systems Incorporated (x86_64)",
            baseurl => "http://linuxdownload.adobe.com/linux/x86_64/",
            gpgkey  => "puppet:///modules/yum/keys/adobe.key",
        }
    }

}


# Add operatingsystem base repositories
#
# === Parameters
#
#     $baseurl:
#         Location of base repository. If not set default mirrorlist
#         will be used.
#
#     $updatesurl:
#         Location of updates repository. If not set default mirrorlist
#         will be used.
#
class yum::repo::base($baseurl=undef, $updatesurl=undef) {

    $osname = inline_template("<%= @operatingsystem.downcase() %>")

    case $::operatingsystem {
        "centos": {
            file { "/etc/yum.repos.d/CentOS-Base.repo":
                ensure => absent,
            }
        }
        "fedora": { }
        default: {
            fail("yum::repo::base not supported in ${::operatingsystem}")
        }
    }

    yum::repo { $osname:
        descr      => "${::operatingsystem} - Base",
        baseurl    => $baseurl,
        mirrorlist => $::operatingsystem ? {
            "centos" => "http://mirrorlist.centos.org/?release=\$releasever&arch=\$basearch&repo=os",
            "fedora" => "https://mirrors.fedoraproject.org/metalink?repo=fedora-\$releasever&arch=\$basearch",
        },
    }

    yum::repo { "${osname}-updates":
        descr      => "${::operatingsystem} - Updates",
        baseurl    => $updatesurl,
        mirrorlist => $::operatingsystem ? {
            "centos" => "http://mirrorlist.centos.org/?release=\$releasever&arch=\$basearch&repo=updates",
            "fedora" => "https://mirrors.fedoraproject.org/metalink?repo=updates-released-f\$releasever&arch=\$basearch",
        },
    }

}


# Add CentOS CR (continuous release) repository
#
class yum::repo::centos-cr {

    tag("bootstrap")

    if $::operatingsystem != "CentOS" {
        fail("CentOS CR repository not supported in ${::operatingsystem}")
    }

    if versioncmp($::operatingsystemrelease, "7") < 0 {
        package { "centos-release-cr":
            ensure => installed,
        }
    } else {
        augeas { "enable-centos-cr-repo":
            context => "/files/etc/yum.repos.d/CentOS-CR.repo/cr",
            changes => "set enabled 1",
        }
    }

}


# Add Dell OMSA repository
#
class yum::repo::dell {

    case $::operatingsystem {
        "centos", "redhat": { }
        default: {
            fail("Dell OMSA repository not supported in ${::operatingsystem}")
        }
    }

    # Required for detecting the correct system hardware via
    # yum. Dell's repo then provide their own yum-dellsysid after
    # installing the repos.
    include yum::repo::epel
    package { "yum-dellsysid":
        ensure  => installed,
        require => Class["yum::repo::epel"],
    }

    case $::operatingsystemrelease {
        /^6\.[0-9]+/: {
            yum::repo { "dell-omsa-indep":
                descr      => "Dell OMSA repository - Hardware independent",
                mirrorlist => "http://linux.dell.com/repo/hardware/latest/mirrors.cgi?osname=el\$releasever&ve&basearch=\$basearch&native=1&dellsysidpluginver=\$dellsysidpluginver",
                gpgkey     => "puppet:///modules/yum/keys/dell-omsa.key",
                require    => Package["yum-dellsysid"],
            }
            yum::repo { "dell-omsa-specific":
                descr      => "Dell OMSA repository - Hardware specific",
                mirrorlist => "http://linux.dell.com/repo/hardware/latest/mirrors.cgi?osname=el\$releasever&basearch=\$basearch&native=1&sys_ven_id=\$sys_ven_id&sys_dev_id=\$sys_dev_id&dellsysidpluginver=\$dellsysidpluginver",
                gpgkey     => "puppet:///modules/yum/keys/dell-omsa.key",
                require    => Package["yum-dellsysid"],
            }
        }
        default: {
            fail("Dell OMSA repository not supported in ${::operatingsystem} ${::operatingsystemrelease}")
        }
    }

}


# Add ELRepo repository
#
# === Parameters
#
#     $baseurl:
#         Location of repository. If not set default ELRepo mirrorlist
#         will be used.
#
class yum::repo::elrepo($baseurl=undef) {

    include yum::common

    if $yum::common::osname != "el" {
        fail("ELRepo repository not supported in ${::operatingsystem}")
    }

    yum::repo { "elrepo":
        descr      => "ELRepo.org Community Enterprise Linux Repository",
        baseurl    => $baseurl,
        mirrorlist => "http://elrepo.org/mirrors-elrepo.el${yum::common::osver}",
        gpgkey     => "puppet:///modules/yum/keys/elrepo.key",
    }

}


# Add Fedora EPEL repository
#
# === Parameters
#
#     $baseurl:
#         Location of repository. If not set default EPEL mirrorlist
#         will be used.
#
class yum::repo::epel($baseurl=undef) {

    include yum::common

    if $yum::common::osname != "el" {
        fail("EPEL repository not supported in ${::operatingsystem}")
    }

    yum::repo { "epel":
        descr      => "Extra Packages for Enterprise Linux ${yum::common::osver} - \$basearch",
        baseurl    => $baseurl,
        mirrorlist => "http://mirrors.fedoraproject.org/mirrorlist?repo=epel-${yum::common::osver}&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/epel${yum::common::osver}.key",
    }

}


# Add Fedora EPEL Testing repository
#
#     $baseurl:
#         Location of repository. If not set default EPEL mirrorlist
#         will be used.
#
class yum::repo::epel-testing($baseurl=undef) {

    include yum::common

    if $yum::common::osname != "el" {
        fail("EPEL Testing repository not supported in ${::operatingsystem}")
    }

    yum::repo { "epel-testing":
        descr      => "Extra Packages for Enterprise Linux ${yum::common::osver} - Testing - \$basearch",
        baseurl    => $baseurl,
        mirrorlist => "http://mirrors.fedoraproject.org/mirrorlist?repo=testing-epel${yum::common::osver}&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/epel${yum::common::osver}.key",
    }

}


# Add Google repository
#
class yum::repo::google {

    yum::repo { "google":
        descr   => "Google - \$basearch",
        baseurl => "http://dl.google.com/linux/rpm/stable/\$basearch",
        gpgkey  => "puppet:///modules/yum/keys/google.key",
    }

}


# Add HP Service Pack for ProLiant repository
#
class yum::repo::hpspp {

    include yum::common

    if $yum::common::osname != "el" {
        fail("HPSPP repository not supported in ${::operatingsystem}")
    }

    yum::repo { "hpspp":
        descr   => "HP Service Pack for ProLiant - \$basearch",
        baseurl => "http://downloads.linux.hp.com/repo/spp/rhel/${yum::common::osver}/\$basearch/current",
        gpgkey  => "puppet:///modules/yum/keys/hpspp.key",
    }

}


# Add RPM Fusion Free repository
#
class yum::repo::rpmfusion-free {

    include yum::common

    yum::repo { "rpmfusion-free":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Free",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=free-${yum::common::osname}-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-free-${yum::common::osname}-${yum::common::osver}.key",
    }
    yum::repo { "rpmfusion-free-updates":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Free",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=free-${yum::common::osname}-updates-released-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-free-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add RPM Fusion Free Testing repository
#
class yum::repo::rpmfusion-free-testing {

    include yum::repo::rpmfusion-free

    yum::repo { "rpmfusion-free-updates-testing":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Free - Test Updates",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=free-${yum::common::osname}-updates-testing-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-free-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add RPM Fusion Nonfree repository
#
# Enabling this will also enable RPM Fusion Free repository.
#
class yum::repo::rpmfusion-nonfree {

    include yum::common
    include yum::repo::rpmfusion-free

    yum::repo { "rpmfusion-nonfree":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Nonfree",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-${yum::common::osname}-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-nonfree-${yum::common::osname}-${yum::common::osver}.key",
    }
    yum::repo { "rpmfusion-nonfree-updates":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Nonfree",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-${yum::common::osname}-updates-released-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-nonfree-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add RPM Fusion Nonfree Testing repository
#
# Enabling this will also enable RPM Fusion Free repository.
#
class yum::repo::rpmfusion-nonfree-testing {

    include yum::repo::rpmfusion-nonfree
    include yum::repo::rpmfusion-free-testing

    yum::repo { "rpmfusion-nonfree-updates-testing":
        descr      => "RPM Fusion for ${yum::common::osname} \$releasever - Nonfree - Test Updates",
        mirrorlist => "http://mirrors.rpmfusion.org/mirrorlist?repo=nonfree-${yum::common::osname}-updates-testing-\$releasever&arch=\$basearch",
        gpgkey     => "puppet:///modules/yum/keys/rpmfusion-nonfree-${yum::common::osname}-${yum::common::osver}.key",
    }

}


# Add Skype repository
#
class yum::repo::skype {

    yum::repo { "skype":
        descr   => "Skype Yum Repository",
        baseurl => "http://download.skype.com/linux/repos/fedora/updates/i586/",
    }

}


# Add Puppetlabs products repository.
#
class yum::repo::puppetlabs {

    include yum::repo::puppetlabs::dependencies

    case $::operatingsystem {
        "fedora": {
            $path = "fedora/f\$releasever/products/\$basearch"
        }
        "centos","redhat": {
          case $::operatingsystemrelease {
            /^7/:    { $path = "el/7/products/\$basearch" }
            default: { $path = "el/\$releasever/products/\$basearch" }
          }
        }
        default: {
            fail("yum::repo::puppetlabs not supported in ${::operatingsystem}")
        }
    }

    yum::repo { "puppetlabs":
        descr    => "Puppet Labs Packages - Products",
        baseurl  => "https://yum.puppetlabs.com/${path}",
        gpgkey   => "puppet:///modules/yum/keys/puppetlabs.key",
        priority => "90",
    }

}


# Add Puppetlabs dependencies repository.
#
class yum::repo::puppetlabs::dependencies {

    case $::operatingsystem {
        "fedora": {
            $path = "fedora/f\$releasever/dependencies/\$basearch"
        }
        "centos","redhat": {
          case $::operatingsystemrelease {
            /^7/:    { $path = "el/7/dependencies/\$basearch" }
            default: { $path = "el/\$releasever/dependencies/\$basearch" }
          }
        }
        default: {
            fail("yum::repo::puppetlabs::dependencies not supported in ${::operatingsystem}")
        }
    }

    yum::repo { "puppetlabs-dependencies":
        descr    => "Puppet Labs Packages - Dependencies",
        baseurl  => "https://yum.puppetlabs.com/${path}",
        gpgkey   => "puppet:///modules/yum/keys/puppetlabs.key",
        priority => "90",
    }

}


# Add Basho's riak repository
#
class yum::repo::riak {

  yum::repo::packagecloud { "basho/riak": }

}


# Add Erlang Solutions repository.
#
class yum::repo::erlang_solutions {

  if $::operatingsystem != 'CentOS' {
    fail("Erlang repository not supported on ${::operatingsystem}")
  }

  yum::repo { 'erlang-solutions':
    descr   => 'Centos $releasever - $basearch - Erlang Solutions',
    baseurl => 'https://packages.erlang-solutions.com/rpm/centos/$releasever/$basearch',
    gpgkey  => 'puppet:///modules/yum/keys/erlang_solutions.key',
  }

}
